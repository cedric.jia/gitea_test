import json
import numpy
from collections import deque

f = open('tundra.dag.json','rb')
data = json.load(f)
nodes = data['Nodes']

waitingList = deque([104])

visited = numpy.full((len(nodes),1), False, dtype=bool)
count = 0
while len(waitingList) > 0:
    cur = waitingList.popleft()
    count = count + 1
    visited[cur] = True
    # print(nodes[cur]["DebugActionIndex"])
    deps = nodes[cur]['Deps']
    for dep in deps:
        if visited[dep] != True:
            # print(dep)
            waitingList.append(dep)

print("total count", count)

depends = []
for i in range(len(nodes)):
    if visited[i]:
        depends = depends + nodes[i]["Inputs"] 
        
print('input count: '+str(len(depends)))
compact = dict.fromkeys(depends)
print('actual count: '+str(len(dict.fromkeys(depends))))

def endCount(files, end, f = 'not set', write = True):
    remain = []
    result = []
    for ff in files:
        if ff.endswith(end):
            result = result + [ff]
        else :
            remain = remain + [ff]
    result.sort()
    if f != 'not set' and write:
        f.write('----------------------------------------\n')
        f.write("end with "+end+" count: "+str( len(result))+'\n')
        x.write('----------------------------------------\n')
        f.write('\n'.join(result) + '\n')
    print("end with "+end+" count: "+str( len(result))+'\n')
    return len(result), remain

x = open("analysis.txt", "w")

dllCount, remain = endCount(compact, '.dll', x)
exeCount, remain = endCount(remain, '.exe', x)
csCount, remain = endCount(remain, '.cs', x)
rspCount, remain = endCount(remain, '.rsp', x, False)

remain.sort()
x.write('----------------------------------------\n')
print("remain folders count: "+str( len(remain))+'\n')
x.write('----------------------------------------\n')

x.write('\n'.join(remain))

x.close()