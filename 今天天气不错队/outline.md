 # Tundra & Bee  
   * [主要内容](#主要内容)
   * [构建流程概况](#构建流程概况)
   * [基本概念](#基础概念)
     * [DAG](#dag文件)
     * [Bee](#bee)
       * [常用指令](#常用指令)
     * [Tundra](#tundra)
       * [实现依赖关系](#实现依赖关系)
       * [增量构建](#增量构建)
       * [demo实例](#-demo)
       * [断点调试](#断点调试)
   * [Perl流程梳理](#perl流程梳理)
     * [Jam脚本流程](#Jam脚本流程)
       * [内容梳理](#内容梳理)
       * [流程图展示](#流程图展示)
     * [关于jam核心参数如何传递给各个build步骤](#关于jam核心参数如何传递给各个build步骤)
     * [Jam其他参数和功能](#Jam其他参数和功能)
   * [构建分析](#构建分析)
     * [流程](#流程)
     * [构建BuildSystem.exe](#buildsystemexe)
       * [json文件分析](#tundradag)
     * [构建buildprogram.exe](#buildprogramexe)
       * [json文件分析](#tundra_buildprogramdagjson-)
         * [节点依赖关系](#节点依赖关系)
         * [构建指令分析](#构建指令分析)
     
## 主要内容

- 构建流程概况：介绍构建概念及相关工具，总结构建流程；
- Perl流程梳理： 介绍相关参数是如何传递到构建流程；
- 构建分析： 分析具体的构建步骤;

## 构建流程概况
按照程序逻辑关系，设计出对应的指令和依赖关系图， 并按顺序执行构建图。
![picture1](Image/2-1-exe.png)

遵循这个流程, Editor构建的大致过程如下:
- Perl 触发构建；
- Bee 前端创建了一个人类可读的构建图，名为 tundra.*.json;
- tundra 启动时, 将json文件翻译成机器可读的.dag 文件;
- tundra2.exe 根据相应的dag文件构建出目标程序;

![picture1](Image/2-1-3.png)

Editor构建的总体流程图如下：
<!-- ![picture1](Image/outline.png) -->
<div align="center">
<img src="Image/outline.png" width="500px"/>
</div>

## 基础概念
为了更好地解释具体构建细节，这里先介绍下相关背景知识。

### Dag文件
Dag文件定义了构建出目标程序所需要的节点，以及节点之间的依赖关系。其本质是一个有向无环图，节点b依赖于节点a,
意味着节点b需要在节点a完成执行。 如果节点d的依赖a,b,c都没有发送变化，则该节点也不需要重新执行。
<!-- ![tundra_build](Image/dag.png) -->
<img src="Image/dag.png"  width="200px"/>

- Dag文件结构

该文件通过`Tools/Unity.BuildSystem/frontend.pl` 脚本生成
![picture1](Image/2-1-2.png)

整个文件共18部分组成, 具体结构如下:

| 文件        | 类型   |  描述  |
| --------   | -----:  | :----:  |
| Nodes      | array  |   操作集合     |
| FileSignatures        |   array  |   构建期间使用的文件集合   |
| GlobeSignatures        |    array  |  构建期间使用的目录  |
| ContentDigestExtensions      | array  |   扫描的文件扩展集合     |
| StructuredLogFileName        |   string  |   生成的日志文件   |
| StateFileName        |    string    |  State文件, 记录先前构建的状态 |
| StateFileNameTmp      | string  |        |
| ScanCacheFileName        |   string  |   扫描的缓存文件   |
| ScanCacheFileNameTmp        |    string   |    |
| DigestCacheFileName        |   string  |  文件摘要缓存 {content hash, mtime, atime} |
| DigestCacheFileNameTmp        |    string   |    |
| NamedNodes      | object  |   节点数量等信息     |
| DefaultNodes        |   array  |   默认节点 (all_tundra_nodes)   |
| SharedResources        |    array   |   构建期间使用的支持资源，用于支持构建操作，例如编译器服务器。将在执行任何需要它的操作之前创建，并在所有需要它的操作完成后销毁|
| Scanners        |   array  |      |
| Identifier        |    string   |  本文件名  |
| BuildTitle        |   string  |   生成的文件名   |
| RelativePathToRoot        |    string   |  相对路径（root）  |

- Dag Node结构

Build过程中的关键节点, 描述了在构建过程中该步骤执行的操作、输入、输出、以及依赖的节点.
其中的关键信息包括:

| 字段名 | 描述                    |
| ------------- | ------------------------------ |
| Annotation     | 执行的操作描述     |
| Action   | **具体执行的指令**     |
| DebugActionIndex      | 本节点索引     |
| Deps   | **依赖的节点索引**     |


### Bee
Bee是一组功能组件（主要代码在/Tools/Bee目录下，可以认为是一个DLL，一个exe如果包括了它，就可以用它来实现特定功能），
它支持使用者利用C#编程的方式构造DAG，并且提供输出DAG，Trigger Tundra等额外功能。
#### 常用指令
| 命令 | 描述                    |
| ------------- | ------------------------------ |
| -v  |  打印每个节点构建过程中的详细信息     |
| -f  | 强制完全构建    |
| -j N| 最多并行构建 N 个节点  |
| -dag-filename= | 指定dag文件名 |
| -verify-up-to-date | 确认给定的目标是否是最新的，是否需要重建 |

例如在`artifacts\BeeStandaloneDriver`目录下，执行`.\Bee.StandaloneDriver.exe -verify-up-to-date`,
可得到下列信息：

![picture1](Image/2-4-bee-command.jpg)

### Tundra
给定一个DAG图，指定一个目标结果文件，按顺序执行DAG图中节点所需要的所有Action，最终生成结果文件。
命令格式：`Tundra2.exe 　dagPath　Output文件`

Tundra 的构建就对应项目[bee](https://github.cds.internal.unity3d.com/unity/bee)的bee_backend项目。入口就是Main.cpp中的main函数（[相应链接]((https://github.cds.internal.unity3d.com/unity/bee/blob/fb0dc4c8324033e6ea60a2a3996b971e7b84c267/bee_backend/src/Main.cpp#L265)))。其并非2020.3构建所用，但原理相同。

Tundra大致的流程为读取参数，读取dag文件，build目标。随后创建一定数量的线程。这些线程用于接受可执行node队列，并执行node。最后主进程继续执行，对树进行解析，按依赖关系将可执行node放入可执行队列。在任务结束后记录dag信息。

#### 实现依赖关系
---
在创建子线程前，运行DriverSelectNodes，按名字选择将要构建的目标node。如果没有则会选择在dag中的默认node放入m_RequestedNodes。

```
void DriverSelectNodes(const Frozen::Dag *dag, const char **targets, int target_count, Buffer<int32_t> *out_nodes, MemAllocHeap *heap)
{
    if (target_count > 0)
    {
        FindNodesByName(
            dag,
            out_nodes, heap,
            targets, target_count, dag->m_NamedNodes);
    }
    else
    {
        BufferAppend(out_nodes, heap, dag->m_DefaultNodes.GetArray(), dag->m_DefaultNodes.GetCount());
    }

    std::sort(out_nodes->begin(), out_nodes->end());
    int32_t *new_end = std::unique(out_nodes->begin(), out_nodes->end());
    out_nodes->m_Size = new_end - out_nodes->begin();
    Log(kDebug, "Node selection finished with %d nodes to build", (int)out_nodes->m_Size);
}
```
随后在创建自线程后，遍历m_RequestedNodes。检查其依赖是否已执行完，或是其输入是可缓存，并且输入缓存已开启。如果满足条件，则将其放入可执行队列。反之检查其依赖节点。递归执行EnqueueNodeWithoutWakingAwaiters。

因为递归执行只在满足依赖节点执行完成时，将节点放入BuildQueue。因此被放入节点必满足互相依赖关系。

```
int EnqueueNodeWithoutWakingAwaiters(BuildQueue *queue, MemAllocLinear* scratch, RuntimeNode *runtime_node, RuntimeNode* queueing_node)
{
    CheckHasLock(&queue->m_Lock);

    if (RuntimeNodeHasEverBeenQueued(runtime_node))
        return 0;

    LogFirstTimeEnqueue(scratch, runtime_node, queueing_node);
    EventLog::EmitFirstTimeEnqueue(runtime_node, queueing_node);
    queue->m_AmountOfNodesEverQueued++;
    RuntimeNodeFlagQueued(runtime_node);

    //enqueueing a node means that we know we need it to complete our build. Some nodes
    //we know can be processed immediately:
    //1) those whose dependencies have all been completed,
    //2) those who are marked as leaf input cacheable && leaf input caching is turned on.
    //
    //we will only put nodes from these two categories on the workstack. Any other nodes
    //we will only mark them as queued, but we don't actually put them on the workstack, since we already
    //know they cannot yet immediately be acted upon. They will be put on the workstack when their dependencies finish.
    int placed_on_workstack_count = 0;
    if (AllDependenciesAreFinished(queue,runtime_node) || IsNodeCacheableByLeafInputsAndCachingEnabled(queue,runtime_node))
    {
        if (AddNodeToWorkStackIfNotAlreadyPresent(queue, runtime_node))
            placed_on_workstack_count++;
    } else {
        //ok, so in this case the queued node is not immediately actionable. so we don't put it on the workstack, but instead queue up all our tobuild dependencies,
        //so that our node becomes immediately actionable in the future. We don't blindly always do this, because in the case where this node is leaf input cacheable
        //we might get a cache hit, and it won't be necessary at all to build any of the dependencies.
        placed_on_workstack_count += EnqueueNodeListWithoutWakingAwaiters(queue, scratch, runtime_node->m_DagNode->m_ToBuildDependencies, runtime_node);
    }

    //leaf input cache hit or not: we always need the toUseDependencies to be produced.
    placed_on_workstack_count += EnqueueNodeListWithoutWakingAwaiters(queue, scratch, runtime_node->m_DagNode->m_ToUseDependencies, runtime_node);

    return placed_on_workstack_count;
}
```

在FinishNode时，会将依赖其的Nodes遍历，将dependencies都已完成的的Node放入队列中。
检查队列m_NodeBacklinks，这个代表有什么节点依赖他，遍历依赖其的节点，检查其是否已经在队列中（RuntimeNodeHasEverBeenQueued），或是其依赖节点是否都完成了。满足条件将其加入构建队列（AddNodeToWorkStackIfNotAlreadyPresent）。如上，因为被放入队列的节点所依赖的节点都必须已完成，所以确保依赖关系满足。

并且在finishNode中，会检已查构建节点数是否等于目标数目，才中断构建。因此构建会确保目标节点都被构建。

#### 增量构建
---
在ProcessNode下的Execute中，CheckInputSignatureToSeeNodeNeedsExecuting会检查其输入文件指纹（CalculateInputSignature）。如果输入文件的创建时间，或者sha与记录不同，代表输入文件有修改，那么该节点需要重新输出。如果没有，那么就不需要。

随后检查m_BuiltNode，其为上次构建信息。如果成功，并且检查输出文件有效，那么该节点就可以不必执行而被定为已完成。

以上确保了，如果输入文件被修改过，那么该节点就会被重新执行（输出文件将会被修改），因此将其输出文件作为输入文件的节点就需要重新执行，从而确保增量构建。

如果输入文件没有被修改，根据dag中的信息，检查输入与输出文件都是有效的，则可跳过该节点。保证不会对不必要执行的节点进行执行。

#### demo实例
---
在目录下写一个文件名为`Build.bee.cs`，用于构建一个简单的helloworld程序。
```
using Bee.Core;
using Bee.CSharpSupport;
using Bee.NativeProgramSupport;

class Build
{
     public static void Main()
    {
        var np = new NativeProgram("helloworld");
        np.Sources.Add("hello.cpp");
        var toolchain = ToolChain.Store.Host();
        var config = new NativeProgramConfiguration(CodeGen.Debug, toolchain, lump: true);
        np.SetupSpecificConfiguration(config, toolchain.ExecutableFormat).DeployTo("build");
    }
}
```
输入看起来只有hello.cpp。其只包含一个主函数，打印helloworld。运行Bee.StandaloneDriver.exe可获取到Bee.dag.json，可以看出一共有5个action，以及他们之间的依赖关系。
|DebugActionIndex|Annotation|ToBuildDependencies|
|---|---|---|
|0|all_tundra_nodes|1,2,3,4,5|
|1|WriteText artifacts/helloworld/debug_Win64_VS2019/_dummy_for_header_discovery||
|2|C_Win64_VS2019 artifacts/helloworld/debug_Win64_VS2019/obj/hello_j47ja.obj|1|
|3|Link_Win64_VS2019 artifacts/helloworld/debug_Win64_VS2019/helloworld.exe (+pdb)|2|
|4|CopyFiles build/helloworld.exe|3|
|5|CopyFiles build/helloworld.pdb|3|

tundra读取dag信息，在DriverSelectNodes中将all_tundra_nodes放入m_RequestedNodes。然后创建处理节点的线程。在EnqueueNodeWithoutWakingAwaiters中，节点0依赖检查没有完成，递归检查节点1，将节点1放入队列。继续看节点2，节点2依赖1，检查节点1，1已经在队列中返回。随后又是检查节点0的依赖节点3，以此类推。最后queue中只有节点1。

在之前创建的Build Thread中，queue检查到队列中有节点，因此开始执行节点1。先检查节点1的所有输入是否是新的，不过节点1没有输入。接下来检查是否之前有过构建（m_BuiltNode），如果没有就立即执行。现在假设没有构建，会检查，其上次构建情况。如果发现上次构建成功，那么这个节点就没有必要构建，但是还是会再次检查构建的输出指纹是否匹配，或者是否有效。

接下来处理节点完成的工作（FinishNode），如果节点完成节点总数和打算构建节点总数相同，通知大家，构建完成了，可以结束了。完成节点1后发现不等于6，所以继续执行。检查队列m_NodeBacklinks，这个代表有什么节点依赖他，找到有2个节点，节点0与节点2。那就拿出节点2检查其是否已经在构建队列中，发现没有继续（RuntimeNodeHasEverBeenQueued）。再确认该节点依赖是否都完成了，没有换下一个节点。节点2，正好没有在队列中，而且其依赖节点1完成了。那就将节点2放入队列中（AddNodeToWorkStackIfNotAlreadyPresent）。接下来该线程就与其他线程一起等待获取到构建队列中的节点。循环往复。节点会按照依赖关系，执行依赖全部成功完成的节点，再寻找依赖其的节点，将可执行节点加入队列。直到构建完成目标节点数，或是受到特殊中断。中断会由主线程监测情况。主线程将本次构建记录到dag中，用于下次构建。

如下是关于增量构建以及依赖关系生成。红色框关于依赖关系生成。而绿色框关于增量构建

![picture7](./Image/BeeMapColored.png)

而下图为为更加大范围的bee_backend运行逻辑

![beemap](./Image/bee.png)

#### 断点调试
---
本文断点调试的项目[bee](https://github.cds.internal.unity3d.com/unity/bee)。本文运行代码使用powershell。

先运行bee，让其完成一次相对完整的构建。
```
.\bee
```
随后找到Bee.StandaloneDriver.exe，切换至其所在目录。按上一步，写好Bee.Build.cs以及实际程序所需源文件。设置环境变量GIVE_DEBUGGER_CHANCE_TO_ATTACH为Bee.StandaloneDriver.exe。
```
cd $DIR
$env:GIVE_DEBUGGER_CHANCE_TO_ATTACH="yes"
.\Bee.StandaloneDriver.exe
```
随后便可看到提示窗口

<img src="Image/TundraDebug.png" width="300px">

随后visual studio打开Bee.gen.sln。找到attach选项。

<img src="Image/attach.png" width="1000px">

找到搜索bee，找到tundra的进程，并attach。

<img src="Image/attach2.png" width="800px">

attach成功，点击先前窗口的ok。程序执行，开始检查流程。

<img src="Image/attached.png" width="1000px">

本文最初用了很多奇怪的操作attach上，不过后来发现，其实在Tundra的主函数入口就有了弹出窗口等待程序的代码。根据如下代码，只要环境变量GIVE_DEBUGGER_CHANCE_TO_ATTACH存在且赋值不为null，tundra会暂停等待attach。

```
#if TUNDRA_WIN32
    if (getenv("GIVE_DEBUGGER_CHANCE_TO_ATTACH") != nullptr)
    {
        MessageBox(
            NULL,
            "Native debugger can attach now",
            "Tundra",
            MB_OK);
    }
#endif
```

## Perl流程梳理
构建最初是由Perl脚本触发, 下面将具体分析下脚本流程。

### Jam脚本流程

#### 内容梳理
该流程主要涉及的文件如下：
- /jam.pl
- /Tools/Build/Tundra.pm
- /Tools/Unity.BuildSystem/frontend.pl
- /Tools/Unity.BuildSystem/Frontend.pm

(注: pm后缀文件为Perl 模块， pl文件为Perl 库文件)


上述各个文件主要作用如下:
1. jam.pl：
主要是做命令行参数的接收,以及执行一些和构建无关的命令，然后调用 Tundra.pm 里面的call_tundra方法并传入frontend.pl路径和接受到的参数以及需要生成的 `/artifacts/BuildProfile/{$cleanedIncomingArguments}.json` 文件，并且输入 `-f` 参数 后会删除 `artifacts/tundra/{$cleanedIncomingArguments}.dag` 和 `artifacts/tundra/{$cleanedIncomingArguments}.dag.json`


 对于上面jam.pl文件里的 `$cleanedIncomingArguments` 变量 相关代码如下：

```
    if ($el =~ /^-sCONFIG=(.*)/)
    {
        $cleanedIncomingArguments .= "Cfg" . $1;
    }
    elsif ($el =~ /^-sPLATFORM=(.*)/)
    {
        $cleanedIncomingArguments .= "Plat" . $1;
    }
    elsif ($el =~ /^-sSCRIPTING_BACKEND=(.*)/)
    {
        $cleanedIncomingArguments .= "Script" . $1;
    }
    elsif ($el =~ /^-sLUMP=(.*)/)
    {
        $cleanedIncomingArguments .= "Lump" . $1;
    }
    elsif ($el =~ /^-s/)
    {
        $incomingArgumentsToHash .= $el;
    }
    else
    {
        $cleanedIncomingArguments .= $el;
    }
```
最后生成的文件名字 就是 $target+$el别称+$el的值这样，例如 `artifacts/BuildProfile/MacEditorCfgdebug.json`。


2. Tundra.pm：
包含两个方法 PrepareTundra 和 call_tundra 。
(1) PrepareTundra 方法是返回tundra2文件 `artifacts/tundra-via-stevedore-in-perl/tundra2(.exe)` ，如果是win就返回.exe 。
(2) call_tundra 方法主要就是根据传入的参数 使用turdra2来执行 ，如果 `$exitcode = 4 ` 就会先删除jam.pl传入的 `{$tundraDagFile}.json` 文件，然后执行jam.pl传入的 `{$frontEndCommand}` 命令，以及设置一些env，如 `TUNDRA_DAGTOOL_FULLCOMMANDLINE` ，`DOWNSTREAM_STDOUT_CONSUMER_SUPPORTS_COLOR` ，`TUNDRA_JSON_FILE`。


对于上面env `TUNDRA_JSON_FILE` ,在 `/Tools/Unity.BuildSystem/Unity.BuildSystem/Main.cs` 里面有段代码
```
var tundraDagJson = Environment.GetEnvironmentVariable("TUNDRA_JSON_FILE");
if (string.IsNullOrEmpty(tundraDagJson))
    throw new Exception("TUNDRA_JSON_FILE is not set");
```
是这样通过env来获取将要生成的dag.json文件的名字和路径


3. frontend.pl：
主要是给tundra来执行的脚本,来生成json文件。
构建分为两个部分，一个是pass1  另一个是真实部分(至于为什么两部分暂时还不清楚)，Unity.BuildSystem.exe能够为它们两部分创建json文件，先生成pass1部分，然后再生成真实部分。
然后根据系统环境使用本地.net或者mono(跨端平台)，来执行Unity.BuildSystem.exe 生成 `/artifacts/tundra/pass1_{$targetName,如MacEditor}_original.dag.json` 和最终文件。

4. Frontend.pm：
里面有方法prepare_build_program  作用是解压准备 Unity.Cecil， il2cpp，
然后返回mono文件地址 `External/MonoBleedingEdge/builds/monodistribution/bin/mono` 中间会设置env `MONO_EXECUTABLE` ，方法最后再删除该env



#### 流程图展示
下图展示了jam脚本的主要流程

![jam流程](Image/2-2-jam.png)


例如 MacEditor

```
jam MacEditor -sCONFIG=debug
```
过程中会生成 

artifacts/BuildProfile/MacEditorCfgdebug.json

artifacts/tundra/MacEditorCfgdebug.dag和
artifacts/tundra/MacEditorCfgdebug.dag.json

artifacts/tundra/pass1_MacEditor_original.dag.json
artifacts/tundra/pass1_MacEditor.dag

artifacts/buildprogram/tundra_buildprogram.dag和
artifacts/buildprogram/tundra_buildprogram.dag.json

artifacts/BuildProfile/MacEditor_frontend.json

artifacts/tundra/MacEditor.dag
artifacts/tundra/MacEditor.dag.json

artifacts/tundra.dag和
artifacts/tundra.dag.json



### 关于jam核心参数如何传递给各个build步骤
主要（平台Development/Debug/Release Build)

查看jam.pl文件 

举例 targetName=MacEditor 和 -sCONFIG=debug
然后命令行输入命令
```
./jam MacEditor -sCONFIG=debug
```

1、首先jam.pl先接收命令行参数，拼接后赋给变量 `$cleanedIncomingArguments` = `MacEditorCfgdebug`

然后调用call_tundra方法传入参数
```
call_tundra($schrootprefix, $dagfile, "perl Tools/Unity.BuildSystem/frontend.pl $arg_string", join(" ", @tundraargs));
```
其中传递给frontend.pl 的参数是$arg_string=`MacEditor -sCONFIG=debug`，另外传递到call_tundra方法里的最后一个值是 `--profile=artifacts/BuildProfile/MacEditorCfgdebug.json convertedjamtargets`

2、 frontend.pl里面 接收传进来的参数 `MacEditor -sCONFIG=debug`,然后再传递给
artifacts/UnityBuildSystem/Unity.BuildSystem/Unity.BuildSystem.exe 去接收

3、`Tools/Unity.BuildSystem/Unity.Buildsystem/Main.cs` 里面 把接收的参数通过
`JamStyleCommandLineParser` 方法 解析，然后设置到env里面

把 target `MacEditor` 设置到  env `JAM_COMMAND_LINE_TARGETS` 里面，可以参考如下相关代码
```
 // Parse the commandline
var cmdLine = new JamStyleCommandLineParser(cmdLineArgs);

GlobalVariables.Singleton["JAM_COMMAND_LINE_TARGETS"].Assign(cmdLine.TargetsToBuild);

foreach (var jamVar in cmdLine.Variables)
    GlobalVariables.Singleton[jamVar.Key].Assign(jamVar.Value);
```

### Jam其他参数和功能
命令格式：
/jam [target...] [-sNAME=VALUE...] [-flags]

一些帮组命令
./jam ---help {targetName}
./jam ---helpjson



| 命令 | 描述                    |
| ------------- | ------------------------------ |
| -sCONFIG=debug/release     |  debug/release 的build     |
| -sPLATFORM=<default>/win64/macos_x64/linux64  | 构建哪个平台上的,指明编译平台特性，比如是32位还是64位    |
| -sSCRIPTING_BACKEND=il2cpp/mono      | 脚本后处理方式 [mono的知乎描述](https://zhuanlan.zhihu.com/p/352463394) 和 [il2cpp的知乎描述](https://zhuanlan.zhihu.com/p/141748334)     |
| -sDEBUG   | 是否允许调试，1为允许，0为不允许（不再使用）     |
| -sOUTPUT_DEPGRAPH  | 是否生成artifacts/depgraph_{buildTargetName}.html（html交互文档）查看依赖关系图。1为生成，0为不生成     |
| -sLUMP=1/0  |      |
| -a |  全部重新构建 |
| -dx/-dax/-dxa/-dg |  打印所有执行操作的命令 |
| -f |  删除artifacts/tundra/xxx.dag和xxx.dag.json文件 然后重新生成 |
| --verify-up-to-date |  （验证更新暂时禁用） |


非构建相关命令
| 命令 | 描述                    |
| ------------- | ------------------------------ |
| jam why     |  解释指定节点为何需要重新构建     |
| jam how   | 解释指定节点是如何构建的    |
| jam time-report      | 解释最近一次构建所需的时间     |
| jam include-report   | 汇总打印最近一次构建 c/c++的头部依赖     |




## 构建分析
介绍完相关背景知识后，我们将会结合 `Bee.StandaloneDriver/StandaloneBeeDriver.cs`文件，从以下几个方面
介绍 Prepare build program 部分的构建流程。
- 分析BuildSystem.exe是如何生成的；
- 分析BuildProgram.exe 是如何生成的；
- 分析buildprogram.exe的Main函数的细节；

### 流程

1. 确保 Tundra 被下载并缓存在 `artifacts/tundra-vis-stevedore`,
解析参数列表, 存储到BeeOptions字段中, 其中extra存放Commend命令参数;
2. 判断是否需要完全构建；
3. InvokeTundra()函数 调用tundra；
4. 当文件列表 (glob 签名）与存储在 DAG中的值不同，会返回代码 4（要求重新运行构建程序);
5. 调用produceJson() 函数构建程序；

<!-- ![tundra_build](Image/2-4-total.png) -->
<div align="center">
<img src="Image/2-4-total.png"  width="500px"/>
</div>

### BuildSystem.exe
可以看出，构建的核心在于执行InvokeTundra()函数：
``` 
输入: tundraDag, produceJson, tundraArgs
目的: 传入相应的参数, 通过produceJson函数, 生成相应的 .dag.json文件, 翻译成相应的 .dag文件。
```

在实际构建过程中，多层嵌套调用InvokeTundra(), 其流程如下：
![tundra_build](Image/2-4-build.png)

BuildProgram.exe构建出来后，Bee调用其Main()函数生成`tundra.dag.json`.
![tundra_build](Image/2-4-bs.png)

#### tundra.dag
在Unity.BuildSystem.BuildProgramForBee.bee.cs文件描述了生成BuildSystem.exe的各个节点
之间的依赖关系， 并依靠此生成`tundra.dag.json`。
以UnityBuildSystem.VisualStudio为例，cs文件中描述了其引用。
![tundra_build](Image/2-4-bscs.png)

与json文件中的Node-4318可一一对应。
![tundra_build](Image/2-4-bsnode.png)
```
 {
  "annotation": "CSProject External/NiceIO/NiceIO.gen.csproj",
  "index": "4318"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.Tools/Bee.Tools.gen.csproj",
  "index": "4319"
 },
 {
   "annotation": "CSProject Tools/Bee/Bee.Core/Bee.Core.gen.csproj",
   "indx": "4324"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.DotNet/Bee.DotNet.gen.csproj",
  "index": "4327"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.VisualStudioSolution/Bee.VisualStudioSolution.gen.csproj",
  "index": "4328"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.CSharpSupport/Bee.CSharpSupport.gen.csproj",
  "index": "4335"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.NativeProgramSupport/Bee.NativeProgramSupport.gen.csproj",
  "index": "4339"
 },
 {
  "annotation": "CSProject Tools/Bee/Bee.Toolchain.VisualStudio/Bee.Toolchain.VisualStudio.gen.csproj",
  "index": "4346"
 },
 {
  "annotation": "CSProject Tools/Unity.BuildSystem/JamSharp.Runtime/JamSharp.Runtime.gen.csproj",
  "index": "4403"
 },
 {
  "annotation": "CSProject Tools/Unity.BuildSystem/Unity.BuildSystem.Common/Unity.BuildSystem.Common.gen.csproj",
  "index": "4405"
 },
```
整个Unity.BuildSystem.exe的相关依赖相对较多，通过分析输入文件。发现输入有10561个, 去重后为1159个。其中dll有230个，exe有4个，rsp有50个，文件夹62个。rsp主要为压缩输入设置，也是用了其他依赖可以不用分析。而文件夹类似。

先看dll，其中有`artifacts/Stevedore/referenceassemblies_v471_a0c3/`下大量文件，主要为.NET v471的资源。`Newtonsoft.Json`用于生成或读取json文件。`Bee.Toolchain.xxx`下主要发行到不同平台所需要使用的依赖库，例如IOS，PS4,Switch等等。NiceIO用于NPath等System.IO相关。`artifacts/UnityBuildSystem`包JamSharp，是unity自己实现的一套Jam的功能，还有一些平台相关的ToolChain。最后就是`artifacts/Bee/Bee.xxx`是bee相关的的内容，包含运行文件生成dag，sln，以及TundraBackend调用，还有一些类似`why`等小功能。

exe文件中，Bee.StandaloneDriver.ref.exe是用于生成dag以及调用tundra2.exe，而stevedore.exe则会帮忙下载依赖库。7za.exe用于解压缩。csc.exe是微软的c# compiler用于编译c#。

cs文件是dll相关更细化的一些输入。还有一些External的音频输出，OpenSSL，il2cpp，jpeg，png，zlib等依赖。

### buildprogram.exe
在多层嵌套中，最先生成的是buildprogram.exe.
这里讨论默认情况下的生成方式. SetupDefaultBuildProgramBuildProgram()会读取`Bee.config`配置文件， 将其中
的BeeFiles作为源文件，指定projectFile路径为 `Tools/Unity.BuildSystem/build.gen.csproj`, 输出路径为
`buildprogram/buildprogram.exe`.
![tundra_build](Image/2-4-bp.png)
同时也把buildprogram.exe 和 build.gen.sln 作为build的依赖，写入 `tundra_buildprogram.dag.json`。

#### tundra_buildprogram.dag.json 分析

##### 节点依赖关系
构建节点的依赖关系如图所示：
![picture1](Image/2-1-1.png)

##### 构建指令分析
本次构建用到的指令如下:
* Download & Unpack: 下载解压相应的资源
* WriteResponseFile: 生成RSP文件
* Csc: 编译
* CSProject: 生成csproj文件
* VisualStudioSolution: 将.csproj文件生成.sln文件

1. 下载工具

   7za:  用于压缩解压文件(根据目标平台, 选择相应的版本)
   
   [referenceassemblies](https://docs.microsoft.com/en-us/dotnet/standard/assembly/reference-assemblies)：程序集采用可执行文件 (.exe) 或动态链接库文件 (.dll) 的形式，是 .NET 应用程序的构建基块.
   
   roslyn-csc：处理rsp文件, 执行编译

2. RSP文件

   一个RSP文件包含C＃使用的一个或多个命令行参数, 它可以存储一个或多个编译器选项以及一个或多个要编译的源代码文件的名称. RSP文件以纯文本格式保存，并在每次编译时由.NET编译器平台（也称为Roslyn）附带的CSC处理. 其目的主要在于简化命令的长度.

   本次构建生成rsp文件位于`/artifacts/rsp`目录下

3. Csc
Roslyn/csc.exe 处理上一步骤生成的rsp文件, 生成相应的结果:
   ```
    buildprogram.exe
    buildprogram.pdb
    buildprogram.ref.exe
   ```

4. CSProject

.csproj 文件记录了与工程有关的相关信息，例如包含的文件，程序的版本，所生成的文件的类型和位置的信息等。有兴趣了解该文件详细信息[请移步](https://blog.walterlv.com/post/understand-the-csproj.html).

生成的c#工程文件位于`Tools/Unity.BuildSystem/build.gen.csproj`, 重点关注其中引用的cs文件，可以看出就是bee.config中配置的文件。
![picture1](Image/2-1-4.png)

以及dll文件：
![picture1](Image/2-1-5.png)

## 致谢
在讨论过程中，与陈智，张光辉同学进行了相应的讨论，并得到了朱星玮导师的帮助。
