# 第二周: Unity构建基础：Bee与Tundra

# Unity构建基础：Bee与Tundra

## 简介

本章节将介绍 Unity 源码构建的基本流程，帮助开发者快速理解本地构建过程中的相关概念，工具以及实现原理。

本文会梳理构建过程中的基本概念，包含构建需要的源材料、构建结果以及 unity 编译所需要的相关工具。在本地构建中，项目使用 Perl 脚本进行订制化、程序化的执行。构建过程主要由两个核心组件组成：bee 和 tundra。文章将依次介绍 bee 和 tundra 的基本功能，分析其机制与实现原理，阐述设计细节，并说明他们在 Unity 构建过程中是如何执行。

阅读完本章节后，开发者能够对 Unity 构建过程建立系统性的认识与理解，并初步熟悉 Bee 和 tundra 的相关功能与实现原理。

## 构建的基本概念

### 基础概念

1. 构建所需要的原材料
- 源代码：基本的程序代码，是应用程序的主体
- stevedore： unity 构建过程中的包管理工具，对外部软件包进行下载、管理等操作
- 编译器：将源代码、目标文件、外部依赖等编译为构建结果，如可执行文件，.dll 动态链接库等
2. 编译相关工具
- Perl 作为最外层的脚本语言，控制流程
- Tundra2.exe 根据依赖关系执行构建
- Bee 可执行程序：生成依赖关系并调用 Tundra2.exe 构建

### 基本流程

Perl 脚本触发构建流程，调用 Bee。 Bee 根据 Perl 脚本传入的参数，生成对应的 dag 图，描述构建的流水线流程 。Bee 生成 build graph 之后，调用 tundra 根据 dag 图，依次执行相应的构建指令，完成构建，输出结果文件。

## Perl 流程控制

使用 Perl 脚本控制构建流程，用户通过参数来执行定制化构建

### 流程梳理

我们对 Jam.pl 脚本做了简单分析，梳理流程如下: 

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled.png)

本章节，我们进一步深入  [frontend.pl](http://frontend.pl) → tundraDagFile.json 这个过程，探索 frontend.pl 是如何生成对应的 Dag json 文件，以及 tundra 是如何读取并具体 action 的。

观察 [frontend.pl](http://frontend.pl) 脚本，我们能发现流程大致如下:

1. prepare_build_program

    执行 `bee_bootstrap.exe`，将 `BuildProgram` 作为目标参数传入

    ```perl

    my $mono = "External/MonoBleedingEdge/builds/monodistribution/bin/mono";
        $ENV{"MONO_EXECUTABLE"} = $mono;
        system("$mono --debug Tools/BeeBootstrap/bee_bootstrap.exe --silence-if-possible BuildProgram") eq 0
            or die("Frontend failed running bee_bootstrap");
        delete $ENV{"MONO_EXECUTABLE"};
    ```

2. 将解析的命令行参数传入，执行 `Unity.BuildSystem.exe`

    ```perl
    $invocation =
        $usedotnet
        ? "artifacts\\UnityBuildSystem\\Unity.BuildSystem\\Unity.BuildSystem.exe @ARGV"
        : "$mono --debug artifacts/UnityBuildSystem/Unity.BuildSystem/Unity.BuildSystem.exe @ARGV";
    system($invocation) eq 0 or die("failed running $invocation");
    ```

到这里，我们遇到3个新名词，`bee_bootstrap.exe` 、`BuildProgram`、`Unity.BuildSystem.exe`

要想进一步探索 Editor 构建过程，就先要了解3者从何而来，以及对应关系

从 prepare_build_program 入手，首先是 `bee_bootstrap.exe` ，它由 Bee 源码编译而来，对应入口为 Tools/Bee/Bee.StandaloneDriver/StandaloneBeeDriver.cs

研究对应代码后发现，它的主要工作为**根据传入的第1个命令行参数，匹配对应的 TopLevelBeeCommand 类，从而执行对应类的逻辑**

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%201.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%201.png)

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%202.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%202.png)

而当我们调用 prepare_build_program 后，也就是执行`system("$mono --debug Tools/BeeBootstrap/bee_bootstrap.exe --silence-if-possible BuildProgram")`  后，对应的TopLevelBeeCommand 为 DefaultSubCommand，即 BuildTopLevelBeeCommand

观察 prepare_build_program 的执行结果，我们可以在 artifacts 目录下看到以下文件:

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%203.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%203.png)

果然，看到我们在上文提到的 `BuildProgram`、`Unity.BuildSystem` 的相关文件

那么这意味着，要回答我们最开始的问题(`bee_bootstrap.exe` 、`BuildProgram`、`Unity.BuildSystem.exe`3者从何而来，以及对应关系)，我们需要进一步研究 BuildTopLevelBeeCommand 

不过在此之前，我们已经可以回答以下2个问题: 

1. Jam还有哪些参数，都有什么功能？
2. Jam的核心参数（平台和Development/Debug/Release Build)是怎么传递给各个Build步骤 

### Jam 其他参数和功能

Jam 脚本除了构建相关的命令 (如 Jam MacEditor)，还支持以下命令:

- jam why

    解释指定节点为何需要重新构建

- jam how

    解释指定节点是如何构建的

- jam time-report

    解释最近一次构建所需的时间

- jam include-report

    打印在先前构建中包含最多的 C++ 头文件的摘要

- jam privacy/internaldocs/steve 等等

从上文可知，这部分功能的实现都对应于各个BuildTopLevelBeeCommand， 找到对应 class 即可进一步研究其执行原理，如: 

```csharp
internal class WhyTopLevelBeeCommand : TopLevelBeeCommand
    {
        public override string Name => "why";
        public override string Abbreviation => "w";
        public override string Description => "explain why the given target was built";
        public override void Execute(string[] args) {}
     }
```

### Jam 核心参数传递

1. 首先 [jam.pl](http://jam.pl) 在执行 Unity.BuildSystem.exe (入口: Tools/Unity.BuildSystem/Unity.BuildSystem/Main.cs) 会将其解析的命令行参数传入

    ```jsx
    my $arg_string = join(" ", @args);
    my $dagfile = "artifacts/tundra/$cleanedIncomingArguments.dag";
    call_tundra($schrootprefix, $dagfile, "perl Tools/Unity.BuildSystem/frontend.pl $arg_string", join(" ", @tundraargs));
    ```

2. JamStyleCommandLineParser 会解析参数，然后存于全局变量

    其中平台参数 buildTarget 以Key:JAM_COMMAND_LINE_TARGETS 的形式存储

    Debug/Release 等字段以Key:CONFIG的形式存储

    ```csharp
    // Parse the commandline
    var cmdLine = new JamStyleCommandLineParser(cmdLineArgs);

    GlobalVariables.Singleton["JAM_COMMAND_LINE_TARGETS"].Assign(cmdLine.TargetsToBuild);

    foreach (var jamVar in cmdLine.Variables)
        GlobalVariables.Singleton[jamVar.Key].Assign(jamVar.Value);
    ```

    ```csharp
    internal static void BuildEntryPoint()
       {
            ((JamBackend)Backend.Current).EnsureFileWritingActionIsSetupWithName("WriteFile");

            var defaultConfig = "debug";
            // WebGL only supports release
            if (Vars.JAM_COMMAND_LINE_TARGETS.Contains("WebGLSupport") || Vars.JAM_COMMAND_LINE_TARGETS.Contains("WebGLSupportAll"))
                defaultConfig = "release";
            if (string.IsNullOrEmpty(Vars.CONFIG))
                Vars.CONFIG = defaultConfig;
    }
    ```

3. 各个构建子模块会在运行中读取该全局变量并做相应操作

## Bee

接上文，本节我们来研究 BuildProgram、UnityBuildSystem 从何而来。在此之前，我们需要先了解 Bee 的几个概念:

1. Bee 本质上是一个构建工具，它通过提供构建系统所需要的组件库(比如依赖工具)，让我们能够以 `C# 代码`的形式来定义我们的构建系统
2. BuildProgram 

    官方[文档](https://internaldocs.hq.unity3d.com/bee_build_system/#frontend)对 builProgram 有如下的定义：在构建流程前端前，有一部分由用户编写的 C# 构建代码，运行这段程序将会输出含有完整构建流程图的 json 文件。这部分代码通常称之为 buildProgram 。

总结如下，Bee 会根据我们自定义的 C# 代码生成 build program，然后运行 build program 生成我们定义的构建产物。

下文将分析上文提到的入口文件：Tools/Bee/Bee.StandaloneDriver/StandaloneBeeDriver.cs

### StandaloneBeeDriver执行过程

上文提到过，StandaloneBeeDriver的第一步工作就是:

根据传入的第1个命令行参数，匹配对应的 TopLevelBeeCommand 类，从而执行对应类的逻辑

而当我们调用 prepare_build_program 后，也就是执行`system("$mono --debug Tools/BeeBootstrap/bee_bootstrap.exe --silence-if-possible BuildProgram")`  后，对应的TopLevelBeeCommand 为 DefaultSubCommand，即 BuildTopLevelBeeCommand

其对应函数入口为

```csharp
public override void Execute(string[] args) => StandaloneBeeDriver.BuildMain(args);
```

对 BuildMain 的执行过程梳理大致如下:

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%204.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%204.png)

1. 加载 Bee.config 文件
2. 准备 tundra 二进制文件，若尚未存在，则从 stevedore 上下载
3. 调用 tundra 程序，传入确定好的 dag 文件名(比如: tundra.dag) 和需要传递给 tundra 的命令行参数 (比如: --silence-if-possible, -p profile.json)
4. 当 tundra 需要 Bee 重新生成 build graph 时 (比如: tundra.dag.json 文件不存在)，会以 exit code 为 4 退出进程
5. 此时会调用 CreateBuildGraph 以生成我们需要的 dag.json 文件
6. CreateBuildGraph 首先会确保 BuildProgram.exe 已经构建好 

    因为从上文我们可知，BuildProgram.exe 是由我们的自定义的 C# build code 编译而来的， 也就是说只有 BuildProgram.exe 知道如何创建我们的 build graph 

    大致构建过程如下: 

    1. 读取 bee.confg 文件中的 BeeFiles 字段，确定用于编译的用户c# build code文件
        - 若设置了 BeeFiles 字段，则通过字段找到对应的 c# 文件. (注意该字段既可以是文件名，也可以使目录名)

            ```csharp
            NPath beeFile = (string)token;
            if (beeFile.FileExists())
            {
                result.Add(beeFile);
                Backend.Current.RegisterFileInfluencingGraph(beeFile);
            }

            if (beeFile.DirectoryExists())
            {
                foreach (var file in beeFile.Files("*.bee.cs", true))
                {
                    result.Add(file);
                    Backend.Current.RegisterFileInfluencingGraph(file);
                }
            }
            ```

        - 若未设置该字段，则读取项目根目录下所有  *bee.cs 文件

            ```csharp
            var filesInRoot = BeeOptions.BuildProgramRoot.Files("*bee.cs", false);
            return filesInRoot.Concat(BeeDirs.SelectMany(d => d.Files("*bee.cs", true))).ToArray();
            ```

    2. 将上一步的得到 c# 作为输入参数，生成 buildprogram.exe 构建所需要的的 build graph 文件，即tundra_buildprogram.dag.json
    3. 运行 tundra，传入 tundra_buildprogram.dag.json， 生成 BuildProgram.exe

7. 调用 buildprogram.exe 以生成本次构建需要的 buildGraph 文件

执行流程大致如下: 

```jsx
var assembly = Assembly.LoadFrom(BuildProgramPath.ToString(SlashMode.Native));
if (assembly.EntryPoint == null)
        throw new InvalidOperationException("BuildProgram has no entry point.");
          
assembly.EntryPoint.Invoke(null, Array.Empty<object>());

tundraBackend.AddInputsFromOtherTundraFileAsFileSignatures(BuildProgramDagFile.ChangeExtension(".dag.json"));
tundraBackend.AddFileAndGlobSignaturesFromOtherTundraFile(BuildProgramDagFile.ChangeExtension(".dag.json"));

tundraBackend.AddExtensionToBeScannedByHashInsteadOfTimeStamp("c", "cpp", "h", "cs", "hpp");
tundraBackend.Write(targetJson);
```

1. Assembly.LoadFrom() 加载 buildprogram.exe
2. 准备/安装 必要工具，graph hook/stevedore
3. EntryPoint.Invoke ()，这里也就是 Bee.exe 调用 BuildProgram.exe 的 Main() 函数，这时候，开始执行并生成我们 c# code 对应的 build graph
4. Invoke 结束后： 
    - tundra_buildprogram.dag.json 加入到 build graph 的 FileSignatures 和 GlobSignatures 字段，将其作为 build graph 的依赖，供 tundra 使用
    - tundra 本身具有重新构建的检查机制，由 timestamp 生成新的文件hash。Bee 在这里设置不同文件类型对应的检查机制

8. 最后，调用 tundra。此时，tundra 需要的 buildGraph 已经创建好了，tundra 会执行对应指令并完成本次构建。

分析结束后，我们可以回答以下提出的问题: 

1. buildProgram.exe 的生成方式？

    bee.exe 通过 Bee.config 获取构建所需要的 tundra_buildprograme.dag.json 文件，运行 tundra

2. buildprogram.exe 编译完成后，Bee 是如何对其调用执行的 ？

    运行过程通过 Load Assembly 加载 buildprogram 的 exe 文件并调用其 Main() 函数

3. 我们定义的目标文件是哪些? Bee 是如何找到并如何编译它们的 ？
    1. bee.config 文件中执行了 c# build code 文件

        ```
        {
          "BuildProgramProjectFilesDirectory": "Tools/Unity.BuildSystem",
          "BeeFiles": ["Tools/Unity.BuildSystem/Unity.BuildSystem.BuildProgramForBeeUnity/BuildProgramForBeeUnity.cs","Tools/Bee/Bee.BuildProgram/BuildProgramForBee.bee.cs"]
        }
        ```

    2. 由上文可知，Bee 通过 bee.config 执行的文件或扫描匹配项目目录下的 *.bee.cs 文件作为 生成 build graph 文件的目标文件

### 构建流程中的依赖分析

**buildProgram.exe**

为了了解生成 BuildProgram.exe 的更多细节，我们尝试通过 tundra_buildprogram.dag.json 并整理其源文件与外部库: 

- 所有指令: dag.json 文件中所有 node 节点

```jsx
0-all_tundra_nodes

1-Download and unpack artifacts/buildprogram/Stevedore/7za-mac-x64_1887/.StevedoreVersion (+1 other)

2-Download and unpack artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/.StevedoreVersion (+1 other)

3-Download and unpack artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/.StevedoreVersion (+1 other)

4-WriteResponseFile artifacts/rsp/16678227308262555357.rsp

5-Csc artifacts/buildprogram/buildprogram.exe (+2 others)

6-CSProject Tools/Unity.BuildSystem/build.gen.csproj

7-VisualStudioSolution Tools/Unity.BuildSystem/build.gen.sln

8-build
```

- 依赖关系

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%205.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%205.png)

    [chart.html](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/chart.html)

- 流程梳理
    - 步骤1- 从 Stevedore 下载操作系统对应的 7za 解压工具
    - 步骤2- 从 Stevedore 下载对应的 .net 系统依赖 dll 文件
    - 步骤3- 从 Stevedore 下载操作系统对应的 c# 编译工具 csc
    - 步骤4- 将编译命令写入临时文件rsp临时文件
    - 步骤5- 读取步骤4中的rsp文件，执行 csc 编译链接，生成 `artifacts/buildprogram/buildprogram.exe`

        源文件包括:

        - Tools/Unity.BuildSystem/Unity.BuildSystem.BuildProgramForBeeUnity/BuildProgramForBeeUnity.cs
        - Tools/Bee/Bee.BuildProgram/BuildProgramForBee.bee.cs

        依赖链接文件包括:

        - bee_bootstrap.exe
        - 步骤2中下载的dll文件
    - 步骤6/7 - 生成 build program 对应的 csproj 和 sln 文件

**Unity.BuildSystem.exe**

为了解 Unity.BuildSystem.exe 的构建细节，尝试分析 artifacts/tundra.dag.json，并整理源文件和外部库

- 所有指令: tundra.dag.json 文件中的 node 节点共计有 4421 个
- 通过 stevedore 下载外部依赖库
    1. 操作系统对应的 7za 解压工具
    7za-win-x64
    2. 指令集合并工具

        ilrepack

    3. .net 系统依赖 dll 文件

        referenceassemblies_v46 \ referenceassemblies_v471

    4. 操作系统对应的 c# 编译工具 csc

        roslyn-csc-win64

    5. 操作系统对应的 python2 环境

        winpython2-x64

- 编译的源文件包括
    1. Configuration/ 目录下的 *.jam.cs 文件
    2. Editor/ 目录下的 *.jam.cs 文件
    3. Extensions/ 目录下的 *.jam.cs 文件
    4. External/ 目录下的 *.jam.cs 文件
    5. External/ 目录下的 *.bee.cs 文件
    6. External/ 目录下的 test 文件
    7. External/TinyProfiler
    8. git
    9. Jamfile 文件
    10. Modules
    11. PlatformDependent 相关依赖（Android、Linux、iPhone etc.）
    12. Platforms 资源文件
    13. Tools/Bee 目录下，bee 的不同模块

## Tundra

### 功能介绍

Unity使用tundra作为后端的构建系统，fork于Andreas Frediksson的[tundra repo](https://github.com/deplinenoise/tundra)。Tundra主要由C++编写，以build graph (dag file)作为输入，支持按dependencies顺序build，多线程并行build，以及增量build。原来的tundra使用lua来生成build graph，而unity的使用bee来生成build graph。

Repo URL:  [https://github.cds.internal.unity3d.com/unity/bee.git](https://github.cds.internal.unity3d.com/unity/bee.git)，代码位于/bee_backend/src下边。

### 执行逻辑

![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%206.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%206.png)

1-3：准备阶段:

- 将传入的参数解析为build option，供后续构建使用。

4：  加载上次构建生成的状态等:

- 包括已经构建的节点，摘要数据，头文件的数据等。这些要加载的文件路径存储在dag文件中，对应StateFileName、ScanCacheFileName、DigestCacheFileName。并且本次build生成的状态也会记录在这些路径。

5：  构建阶段

- 首先会根据参数启动相应数量+1的 thread，如—thread=3，则共有4个 thread，3个 worker + 1个 main thread，每个 thread 会不断的去 workstack 里拿 node 并执行，main thread 则会监听最终构建结束的 signal，以及 Console interrupt signal等。
- 遍历当前所有的 nodes，如果当前 node 的 dependencies 全部完成，或者当前 node 可以由 leaf input 缓存，则放入 workstack。否则，递归的执行对当前节点的所有 dependencies 执行当前操作。这步执行完之后，所有可以立即执行的 node 会被放入 workstack。这就实现了顺序build。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%207.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%207.png)

- 根据每个节点的分数将当前 workstack 排序，一个 node 的分数由当前 node 被多少其他 node 所依赖而决定，分数高的 node 会被放到 stack 的末尾优先执行。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%208.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%208.png)

- Worker 开始 processNode。主要是三个步骤:
    1. 判断所有依赖是否都成功完成, 若未全部完成，把依赖放入到workstack中。
    2. 增量 build: 执行 node 指令，检查 Signature 等决定是否需要真正执行。
    3. 顺序 build: 结束执行，把父节点放入workstack。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%209.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%209.png)

    在 executeNode 方法中，我们可以看到如下逻辑，这也是 tundra 实现增量更新的关键。在calculateInputSignature 中可以看到，通过当前的 node 的 action，inputfile 的 path，inputfile 的文件内容或者 last modified time 等来进行 hash，将生成的 input signature 与读取到的上次 build 的 input signature 进行对比，如果一致，则跳过这个 node 的构建。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2010.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2010.png)

- 如果不需要 build 当前 node，则会进一步检查 buildProgram 的 timestamp 与 globSignatures 所引用的文件的签名变化与否，若有变化，或返回相应的 status 告诉 bee 重新生成 buildgraph。
- 当前 node 执行完成，会进入 finishNode 方法，找到依赖于当前 node 的父节点，如果父节点的所有依赖都执行成功，则把父节点加入到 workstack 等到被 thread 捡起执行。如果当前执行失败，则会立即返回 failed 的 status。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2011.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2011.png)

- 重复上述步骤，直到所有的 node 都被执行成功，通知 main thread。下图是 finishNode 中判断是否所有 node 都执行成功的逻辑。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2012.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2012.png)

6-7： 保存此次构建状态等

- build成功后，结果会一路返回到main.cpp中，此时会把构建过程中的一些状态保存在文件里，并将最终构建结果返回到调用入口。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2013.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2013.png)

至此，一次tundra的调用流程结束。

### Tundra 的依赖关系

1. 由 code 可以看出，NativeProgram bee_backend 除了依赖 bee_backend/src/Main.cpp之外，还使用了 NativeProgramAsLibrary 依赖另一个 NativeProgram lib_backend，而 lib_backend 则依赖 bee_backend/src下除了Main.cpp之外的所有文件。
2. Debug tundra：
执行 ./bee，可以在./build/Distribution/bee_backend目录下看到新生成的 bee_backend 可执行文件。

    ![%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2014.png](%E7%AC%AC%E4%BA%8C%E5%91%A8%20Unity%E6%9E%84%E5%BB%BA%E5%9F%BA%E7%A1%80%EF%BC%9ABee%E4%B8%8ETundra%207dffe8bba9f8420d8103ef443912b827/Untitled%2014.png)