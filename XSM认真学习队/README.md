# Unity构建基础

## tundra_buildprogram.dag.json的指令和依赖关系

**Nodes**:

1. Download and unpack artifacts/buildprogram/Stevedore/7za-mac-x64_1887/.StevedoreVersion (+1 other)
**Action**:

    ```bash
    mono --debug '../unity/Tools/BeeBootstrap/bee_bootstrap.exe' steve internal-unpack public 7za-mac-x64/95bf0d3a17b5_1887c56255e2db1134e118f27abe1ec62a2aca092ec9f72600c80659da17ca11.zip "artifacts/buildprogram/Stevedore/7za-mac-x64_1887"
    ```

    输出

    - artifacts/buildprogram/Stevedore/7za-mac-x64_1887/.StevedoreVersion
    - artifacts/buildprogram/Stevedore/7za-mac-x64_1887

2. Download and unpack artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/.StevedoreVersion (+1 other)
**Action**:

    ```bash
    mono --debug '../unity/Tools/BeeBootstrap/bee_bootstrap.exe' steve internal-unpack public referenceassemblies_v471/1_a0c33a9a16a4ad77c158aac7cd7e47daed3e68b180c65e14474f44c9501c4fae.7z "artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3"
    ```

    输入

    - artifacts/buildprogram/Stevedore/7za-mac-x64_1887/7za

    输出

    - artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/.StevedoreVersion
    - artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3

    依赖: Node1

3. Download and unpack artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/.StevedoreVersion (+1 other)
**Action**:

    ```bash
    mono --debug '/Users/coffey/unityProjects/unityLearn/unity/Tools/BeeBootstrap/bee_bootstrap.exe' steve internal-unpack public roslyn-csc-mac/8da8ba0cf075_43f799e85461479087dd2c2be93c600adeea8eae67e7570b1b5ef7b9952ab0ad.7z "artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7"
    ```

    输入

    - artifacts/buildprogram/Stevedore/7za-mac-x64_1887/7za

    输出

    - artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/.StevedoreVersion
    - artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7

    依赖: Node1

4. WriteResponseFile artifacts/rsp/16678227308262555357.rsp
将WriteTextFilePayload字段中的内容写入目标文件
输出 artifacts/rsp/16678227308262555357.rsp

5. Csc artifacts/buildprogram/buildprogram.exe (+2 others)
**Action:**

    ```bash
    "artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/csc" /nostdlib /noconfig /shared @"artifacts/rsp/16678227308262555357.rsp"
    ```

    输入

    - Tools/Unity.BuildSystem/Unity.BuildSystem.BuildProgramForBeeUnity/BuildProgramForBeeUnity.cs
    - Tools/Bee/Bee.BuildProgram/BuildProgramForBee.bee.cs
    - /unity/Tools/BeeBootstrap/bee_bootstrap.exe
    - artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/中的一些dll文件
    - artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/csc
    - artifacts/rsp/16678227308262555357.rsp

    输出

    - artifacts/buildprogram/buildprogram.exe
    - artifacts/buildprogram/buildprogram.pdb
    - artifacts/buildprogram/buildprogram.ref.ex

    依赖: Node2, Node3, Node4

6. CSProject Tools/Unity.BuildSystem/build.gen.csproj
生成build.gen.csproj文件
输出
    - Tools/Unity.BuildSystem/build.gen.csproj

7. VisualStudioSolution Tools/Unity.BuildSystem/build.gen.sln
生成build.gen.sln文件
输出
    - Tools/Unity.BuildSystem/build.gen.sln

    依赖: Node6

8. build 构建项目
依赖: Node5, Node7

节点之间的关系图：

![image](IMG/dagMap.png)

## jam.pl流程

设置BEE_STEVEDORE_META_MANIFEST环境变量

- 解析脚本命令行参数
 ```--tundra```参数,将```$seenTundraFlag```置为1,之后的参数都将添入```@manualtundraargs```数组, 它覆盖了传递给Tundra的参数, 主要参数在bee_backend>src>Main.cpp中有详细描述, 主要的有:

 ```
  g_OptionTemplates[] = {
     {'j', "threads", OptionType::kInt, offsetof(DriverOptions, m_ThreadCount), "Specify number of build threads"},
     {'t', "show-targets", OptionType::kBool, offsetof(DriverOptions, m_ShowTargets), "Show available targets and exit"},
     {'v', "verbose", OptionType::kBool, offsetof(DriverOptions, m_Verbose), "Enable verbose build messages"},
     ...
     {'D', "debug", OptionType::kBool, offsetof(DriverOptions, m_DebugMessages), "Enable debug messages"},
     ...
     {'R', "dagfile", OptionType::kString, offsetof(DriverOptions, m_DAGFileName), "filename of where tundra should store the mmapped dag file"},
     {'O', "dagfilejson", OptionType::kString, offsetof(DriverOptions, m_DagFileNameJson), "Filename of the json to bake (only used in explicit baking mode)"},
     ...
     {'h', "help", OptionType::kBool, offsetof(DriverOptions, m_ShowHelp), "Show help"},
 #if defined(TUNDRA_WIN32)
     ...;
 ```

  ```-dax, -a```添加至```@tundraargs```数组中,

 `-dax`: 打印完整的每个构建步骤命令行

 `-a`: 不使用之前的构建结果, 重新构建

 `-f`: 删除dag, dag.json文件,使Tundra重新生成构建图


  ```-sCONFIG, -sPLATFORM, -sSCRIPTING_BACKEND, -sLUMP```参数将按指定字符串进行拼接,赋予```$cleanedIncomingArguments```标量,
  除特定参数,其他参数都拼接在```$cleanedIncomingArguments```标量末尾并添入```@args```数组中.

 `-sCONFIG`用于指定区分Debug/Release构建.


  当第一个命令行参数为```why, how , time-report, include-report```时, 将由Bee.StandaloneDriver.exe执行指令


 ```BuildSystemProjectFiles```参数,执行```mono Tools/BeeBootstrap/bee_bootstrap.exe BuildSystemProjectFiles```

- 环境处理
  darwin: 删除SDKROOT环境变量
  linux: SetupLinuxSchroot()

- 变量处理
  ```$profileName = 'artifacts/BuildProfile/' . $cleanedIncomingArguments . '.json'```添加至```@tundraargs```,


  ```@manualtundraargs```数组元素添加至```@tundraargs```中,当```@manualtundraargs```为空时, 将```convertedjamtargets```添加到```@tundraargs```中


  ```@args```通过join合并为```$arg_string```标量, 将传入call_tundra()之中
  `call_tundra($schrootprefix, $dagfile, "perl Tools/Unity.BuildSystem/frontend.pl $arg_string", join(" ", @tundraargs));`

- call_tundra()
  接收参数: ```$schrootprefix, $tundraDagFile, $frontEndCommand, $tundraArgs```

  `schrootprefix`: 非linux系统为空字符串

  `tundraDagFile`: MacEditor or WinEditor.dag文件

  `frontEndCommand`:  perl frontend.pl targetEditor

  `tundraArgs`: -l -v --profile等


  调用PrepareTundra()获取tundra2可执行文件


  执行```my $finalcmd = $schrootprefix . "$tundraPath -R $tundraDagFile $tundraArgs";```, 初次执行返回码为4,这是由于缺少重要文件,如MacEditor.dag, pass1_MacEditor.dag等, 将调用```$frontEndCommand```生成, 通常这个指令为```perl Tools/Unity.BuildSystem/frontend.pl MacEditor/WinEditor```, 此指令调用成功后再调用finalcmd.

call_tundra堆栈图：

![image](IMG/call_tundra1.png)

![image](IMG/call_tundra2.png)



##frontend.pl主要代码

```
...
my $invocation = (
    $usedotnet
    ? "artifacts\\UnityBuildSystem\\Unity.BuildSystem\\Unity.BuildSystem.exe"
    : "$mono --debug artifacts/UnityBuildSystem/Unity.BuildSystem/Unity.BuildSystem.exe"
    )
    . ' -sTUNDRA_DAG_FILE=artifacts/tundra/pass1_'
    . $targetName
    . "_original.dag.json -sJAMFILE=Pass1 -sUNPACK_FOR_TARGET=$targetName Pass1"
    . $customNDK;

system($invocation) eq 0 or die("failed running $invocation");
...

call_tundra("", "artifacts/tundra/pass1_" . $targetName . ".dag", "$copy $copyargs", "--silence-if-possible");

...
$invocation =
    $usedotnet
    ? "artifacts\\UnityBuildSystem\\Unity.BuildSystem\\Unity.BuildSystem.exe @ARGV"
    : "$mono --debug artifacts/UnityBuildSystem/Unity.BuildSystem/Unity.BuildSystem.exe @ARGV";
system($invocation) eq 0 or die("failed running $invocation");
```



## StandaloneBeeDriver中的函数逻辑


### Main函数
将执行时接收到的args参数传递给RealMain函数，通过try方式捕获所有之后执行中发生的错误并输出信息。

### RealMain函数
对输入的参数args进行格式处理，最终返回```List<string>```类型List，通过```FirstOrDefault```方法获取第一个参数信息。
- 如果args[0]为```--help``` 或 ```-h``` 则剔除args[0]后将剩余参数传递给 ```ShowBeeHelp``` 函数。

- 如果args[0]为 ```--version``` 则 ```ShowBeeVersion``` 函数显示Bee相关版本信息。

- 如果args[0]为null则执行在头部定义的```DefaultSubcommand```也就是build指令。

- 否则将在```Subcommands```中通过Name或Abbreviation的方式来匹配args[0]，并剔除args[0]后将剩余参数传递给对应的```Subcommands```进行执行。

### ShowBeeVersion函数
通过```TelemetrySupport.BeeBranchInfo```输出Bee的相关版本信息。可了解到作者为同样是Tundra的开发者的[Andreas Fredriksson](https://github.com/deplinenoise/)大佬。

### ShowBeeHelp函数
该函数会先调用ShowBeeVersion函数，输出Bee的相关版本信息。

判断传递来的args的长度：
- 如果长度大于0则继续在```Subcommands```中通过Name或Abbreviation的方式来匹配args[0]，将匹配中的TopLevelBeeCommand的Description进行返回输出。

- 如果args为空则循环整个通过Name正向排序的```Subcommands```，获取其Abbreviation（缩写）、Name（名称）以及Description（简介）进行输出。并将在最后输出```DefaultSubcommand.Name```告知Bee的默认执行Command也就是```build```指令。

### LoadConfiguration函数
加载本地Bee配置信息。

尝试在传递过来的NPath类型root目录下搜索```bee.config```文件，如果存在将文件读入并转换为json类型的JObject对象。

尝试在传递过来的NPath类型root目录下搜索```bee.local```文件，如果存在将文件读入并转换为json类型的JObject对象并与```bee.config```做Merge合并。

返回最终的JObject。

### BuildMain函数
1. 开启git分支信息检测，通过args配置BeeOptions
2. 加载编译配置信息，设置好编译目录。
3. 检测并设置终端输出是否支持颜色。
4. 检测Tundra可执行程序是否在环境变量中定义，如果未定义先进行下在Tundra并初始化。
5. 获取本次BeeOptions中的需要构建的dag文件及编译目标。
6. 调用InvokeTundra进行构建。
7. 输出Bee Why的解释信息。


## Bee、Tundra、BuildProgram的调用细节
1. 启动Bee
2. Bee通过InvokeTundra方法启动Tundra去尝试构建项目
3. Tundra没有发现tundra.dag文件，启动Bee去调用CreateBuildGraph创建对应的tundra.dag
4. CreateBuildGraph方法中由于tundra_buildprogram.dag文件不存在再去调用CreateGraphForBuildProgram去产生tundra_buildprogram.dag和BuildProgram
5. 启动BuildProgram去产生tundra.dag文件。
6. 再通过Tundra进行最终的编译输出

之后再次编译时Bee调用Tundra就会直接检测tundra.dag文件是否有变化，如果无变化就直接进行目标构建。

## Bee是如何用参数指定生成结果的目标文件的

```
./bee how hello.o

====( This is how C_Mac64 artifacts/helloworld/debug_macosx64/hello.o (in artifacts/tundra.dag.json) is built: )==============================

====( Inputs )==============================
/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++
/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/libtool
artifacts/rsp/721456543370224757.rsp (output of WriteResponseFile artifacts/rsp/721456543370224757.rsp)
hello.cpp

====( Command line )==============================
"/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++" @"artifacts/rsp/721456543370224757.rsp"

====( Response file artifacts/rsp/721456543370224757.rsp )==============================
-std=c++11 -fvisibility=hidden -msse2
--sysroot="/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"
-fno-exceptions -fno-rtti -g -O0 -fno-strict-aliasing -ffunction-sections -fdata-sections -fmessage-length=0 -pipe
-D_DEBUG
-I"."
-o "artifacts/helloworld/debug_macosx64/hello.o"
-fcolor-diagnostics -stdlib=libc++ -arch x86_64 -mmacosx-version-min=10.8
-c -x c++ "hello.cpp"
```
使用how指令查看目标文件编译的过程参数。
可以看到连接了Toolchains、clang++ 、libtool、.cpp源码文件以及相应.rsp中间文件。

通过执行clang++命令加入rsp文件编译对应的cpp文件，最终生成目标文件。


## Tundra构建

Tundra是一个比较独立的工具：它的功能就是，给定一个DAG图以及一个结果文件，按顺序执行DAG图上这个结果节点所需要的所有Action，最终生成结果文件。

命令格式：Tundra.EXE 　DAG图Path　需要的Output文件

第一周我们尝试输出最后执行Tundra的指令，得到结果如下：
```
artifacts/tundra-via-stevedore-in-perl/tundra2.exe -R artifacts/tundra/WinEditor.dag --profile=artifacts/BuildProfile/WinEditor.json convertedjamtargets
```
Tundra构建的特点，根据依赖关系构建，增量式构建
例如：

![image](IMG/Tundra_dag.png)

Tundra解析dag文件，拿到所有的节点信息，就会依次执行”download a.dll from stevedore”， “gcc a.dll p.c p.h -> dependence.dll”，当执行“gcc temp.exe dependence.dll ->final.exe” 时Tundra检查发现当前节点AllDependenciesAreFinished函数返回false，于是依赖节点被放入构建队列中，将被立刻执行，对于依赖节点，依旧如此类推，则可以逐层找到最初的那个节点，或者某个节点的所有依赖节点都已经完成了，这个节点会被添加到真正的工作栈上。因此后续的执行步骤不停地套娃直到最初的节点“download b.lib from stevedore”，依次往下，“gcc b.lib e.c -> d.dll”，“gcc d.dll k.c -> start.exe”，再次来到“gcc temp.exe dependence.dll ->final.exe” 时校验通过，正常执行，最终生成final.exe。
套娃关键函数：

```
static int EnqueueNodeListWithoutWakingAwaiters(BuildQueue* queue, MemAllocLinear* scratch, const FrozenArray<int32_t>& nodesToEnqueue, RuntimeNode* enqueueingNode)
{
    int placed_on_workstack_count = 0;
    for(int32_t depDagIndex : nodesToEnqueue)
    {
        placed_on_workstack_count += EnqueueNodeWithoutWakingAwaiters(queue, scratch, &queue->m_Config.m_RuntimeNodes[depDagIndex], enqueueingNode);
    }
    return placed_on_workstack_count;
}
```
```

int EnqueueNodeWithoutWakingAwaiters(BuildQueue *queue, MemAllocLinear* scratch, RuntimeNode *runtime_node, RuntimeNode* queueing_node)
{
    CheckHasLock(&queue->m_Lock);

    if (RuntimeNodeHasEverBeenQueued(runtime_node))
        return 0;

    LogFirstTimeEnqueue(scratch, runtime_node, queueing_node);
    EventLog::EmitFirstTimeEnqueue(runtime_node, queueing_node);
    queue->m_AmountOfNodesEverQueued++;
    RuntimeNodeFlagQueued(runtime_node);

    int placed_on_workstack_count = 0;
    if (AllDependenciesAreFinished(queue,runtime_node) || IsNodeCacheableByLeafInputsAndCachingEnabled(queue,runtime_node))
    {
        if (AddNodeToWorkStackIfNotAlreadyPresent(queue, runtime_node))
            placed_on_workstack_count++;
    } else {
        placed_on_workstack_count += EnqueueNodeListWithoutWakingAwaiters(queue, scratch, runtime_node->m_DagNode->m_ToBuildDependencies, runtime_node);
    }

    placed_on_workstack_count += EnqueueNodeListWithoutWakingAwaiters(queue, scratch, runtime_node->m_DagNode->m_ToUseDependencies, runtime_node);

    return placed_on_workstack_count;
}
```
EnqueueNodeListWithoutWakingAwaiters传入当前节点的依赖节点index数组，函数foreach获取每个index对应的runtime节点，调用EnqueueNodeWithoutWakingAwaiters，如果不满足条件，会调用EnqueueNodeListWithoutWakingAwaiters，传入参数是当前节点的依赖节点index数组。


再比如执行完成后，我们修改了文件e.c，重新运行Tundra，左边两个节点在执行时经判断未发生改变，不会重新构建，当遍历到“gcc temp.exe dependence.dll ->final.exe”时,在函数ExecuteNode开头位置：
```
bool haveToRunAction = CheckInputSignatureToSeeNodeNeedsExecuting(queue, thread_state, node);
    if (!haveToRunAction)
    {
        EventLog::EmitNodeUpToDate(node);
        return AreNodeFileAndGlobSignaturesStillValid(node, thread_state)
            ? NodeBuildResult::kUpToDate
            : NodeBuildResult::kUpToDateButDependeesRequireFrontendRerun;
    }
```
 返回结果NodeBuildResult::kUpToDateButDependeesRequireFrontendRerun，暂时也不需要重新执行，继续向下遍历，“download b.lib from stevedore”，到“gcc b.lib e.c -> d.dll”时发现e.c的时间戳发生了变化，于是重新执行，然后再重新执行“gcc d.dll k.c -> start.exe”，“gcc temp.exe dependence.dll ->final.exe”，最终生成新的final.exe。


### 整体构建流程介绍

整体构建流程分以下几大步骤：

* 获取并解析命令行输入的参数，进行初始化各种设置
* 根据文件名获取文件信息，并存放在FileInfo中
* 从文件中读取jsonObject
* 将jsonObejct转换成dag节点列表
* 更新dag缓存文件
* 初始化所有线程的状态并启动线程
* 在线程中循环构建所有需要的dag节点
* 构建结束，得到构建结果

![image](IMG/Tundra_build.png)

首先，结构体FileInfo中会记录文件的大小，时间戳以及标志文件类型状态的flag。这是用来判断dag文件是否发生改变的重要依据。此处不得不提到一个名词dagderived_filename，它是用来保存最新一次dag信息的文件的名称，在获取解析dag文件的过程中，Tundra会去判断是否存在，或者是否是最新的，如果不是的话就将最新的dag依赖关系信息写入到这个文件中作为最新的缓存数据。FileInfo中存放的是dag文件的状态信息，而不是具体dag节点的信息，具体的节点状态记录在结构体RuntimeNode中
```
struct RuntimeNode
{
    uint16_t m_Flags;
    uint32_t m_DagNodeIndex;
#if ENABLED(CHECKED_BUILD)
    const char *m_DebugAnnotation;
#endif
    const Frozen::DagNode *m_DagNode;
    const Frozen::BuiltNode *m_BuiltNode;

    NodeBuildResult::Enum m_BuildResult;
    bool m_Finished;
    HashDigest m_CurrentInputSignature;

    DynamicallyGrowingCollectionOfPaths* m_DynamicallyDiscoveredOutputFiles;
    LeafInputSignatureData* m_CurrentLeafInputSignature;
    HashSet<kFlagPathStrings> m_ImplicitInputs;
};
```
结构体RuntimeNode是构建过程中非常重要的一环，这里存放这节点的flags,index,节点信息，构建节点对应的输入文件，输入签名，以及输出结构等，其中有两个重要的参数：
* m_BuiltNode 标志着当前节点是否曾经被构建过，值得注意的是，即便已经被构建过，也有可能构建结果错误或者输出文件遗失等异常情况，或者是设置参数要求不使用先前的构建结果，也需要被重新构建。
* m_Finished 标志着当前节点所依赖的节点是否已经构建完成。这里也有个需要注意的点，完成构建并不意味这一定就构建成功了。

在成功获取dag节点列表信息并成功加载到内存中后，就可以开始正式的构建过程了。

### 依赖关系顺序构建

DriverBuild->BuildQueueInit->BuildThreadRoutine->BuildLoop->ProcessNode函数是检查当前节点所有依赖的节点是否都完成了的逻辑，也是Tundra根据依赖关系顺序构建的核心。具体流程图如下：

![image](IMG/ProcessNode.png)

在一个节点的Action执行完成，最后调用FinishNode函数，```node->m_Finished = true;```,当下一个依赖当前节点的新节点执行Action时，首先调用AllDependenciesAreFinished函数，具体实现如下：
```
static bool AllDependenciesAreFinished(BuildQueue *queue, RuntimeNode *runtime_node)
{
    for (int32_t dep_index : queue->m_Config.m_DagDerived->m_CombinedDependencies[runtime_node->m_DagNodeIndex])
    {
        RuntimeNode *runtime_node = GetRuntimeNodeForDagNodeIndex(queue, dep_index);
        if (!runtime_node->m_Finished)
            return false;
    }
    return true;
}
```
可以看到，函数遍历了所有依赖的节点，并判断runtime_node->m_Finished属性，只有当所有依赖节点都完成了，才会返回true。否则会将当前节点添加到队列中，但并不会放在真正的工作栈上，只有当所有的依赖节点都构建完成了，才会真正被放到工作栈中。当所有依赖节点都完成了，然后会再调用AllDependenciesAreSuccesful函数，它与AllDependenciesAreFinished函数几乎相同，但是在后者基础上对节点的构建结果进行了判断
```
 if (runtime_node->m_BuildResult != NodeBuildResult::kRanSuccesfully && runtime_node->m_BuildResult != NodeBuildResult::kUpToDate)
            return false;
```
也就是说，即便是节点完成构建了，也不一样是成功的，也有可能发生了构建错误，只是完成了构建而已。不过无论AllDependenciesAreSuccesful函数返回的结果如何，最终都会执行FinishNode函数，将当前节点的m_Finished设为true。换言之，m_Finished只能标志当前节点Action是否执行完成了，并不意味着执行结果一定是正确的。比如说下文中增量式构建中未生成文件的时间戳大于系统时间，就会被标记为可能存在错误。

### 增量式构建

我们有一个 RuntimeNodes 数组和一个 BuiltNodes 数组。我们将把它们中的大部分写入一个新的冻结的 AllBuiltinNodes 文件，该文件将成为下一次构建的 BuiltNodes，存放在Bee.dag_derived中。
我们需要维护 BuiltNodes 按构建节点的 guid 排序的不变量。为了做到这一点，我们将同时遍历两个输入数组。两个输入数组也保证已经排序，所以我们只需要确保我们根据 guid 排序顺序交错两个数组。主要的逻辑在函数DriverPrepareNodes中：
```
if (const Frozen::AllBuiltNodes *all_built_nodes = self->m_AllBuiltNodes)
    {
        const Frozen::BuiltNode *built_nodes = all_built_nodes->m_BuiltNodes;
        const HashDigest *state_guids = all_built_nodes->m_NodeGuids;
        const int state_guid_count = all_built_nodes->m_NodeCount;

        for (int i = 0; i < node_count; ++i)
        {
            const HashDigest *src_guid = dag_node_guids + i;
            if (const HashDigest *old_guid = BinarySearch(state_guids, state_guid_count, *src_guid))
            {
                int state_index = int(old_guid - state_guids);
                out_nodes[i].m_BuiltNode = built_nodes + state_index;
            }
        }
    }
```
此刻，当前构建所需的节点如果有可以复用的构建结果，则可以根据m_BuiltNode属性找到。

Tundra是根据输入文件的签名来判断当前节点是否需要重新执行，在执行某个节点时，首先判断输入文件是否具有签名，如果没有，那么说明当前节点是首次被构建，如果有签名，还需要判断是否构建结果错误或者输出文件丢失或者设置不使用以前的构建结果。具体的流程图如下：

![image](IMG/ExecuteNode.png)

首先会执行CheckInputSignatureToSeeNodeNeedsExecuting函数判断是否需要重新执行，判断依据之一便是m_BuiltNode是否有值，当然还有其他的判断，这里代码较多，就不贴出来了。

即便是判断需要重新构建的节点，还需要未曾生成的输入文件生成新的时间戳，然后与文件系统记录的当前时间进行比较，这里所描述的“当前时间”，并不是当前非常精准的时间点，windows有个特性，文件的修改信息每8秒才会刷新到硬盘中，除非有其他进程也打开了文件，这也就是说，我们可能上无法及时地看到文件的修改。因此，为了更新当前文件时间戳，这里其实做了一个写文件的操作，写什么内容并不重要，重要的是要写了点什么。由于这个特性的存在，未生成的输入文件的时间戳一定是小于等于“当前时间”才合理，如果等于“当前时间”，那么我们等待下一个文件系统的tick，这样做的原因是能够安全地检测到dag图的执行期间或者两次tundra执行的变动。如果时间戳大于“当前时间”，则标记为可能出错的文件。
```
uint64_t FileSystemUpdateLastSeenFileSystemTime()
{
    MutexLock(&s_LastSeenFileSystemTimeLock);

    uint64_t valueToWrite = FileSystem::g_LastSeenFileSystemTime; // not important what we write, just that we write something.
    FILE* lastSeenFileSystemTimeSampleFileFd = fopen(s_LastSeenFileSystemTimeSampleFile, "w");
    if (lastSeenFileSystemTimeSampleFileFd == nullptr)
        CroakErrno("Unable to create timestamp file '%s'", lastSeenFileSystemTimeSampleFileFd);

    fwrite(&valueToWrite, sizeof valueToWrite, 1, lastSeenFileSystemTimeSampleFileFd);
    fclose(lastSeenFileSystemTimeSampleFileFd);

    FileSystem::g_LastSeenFileSystemTime = GetFileInfo(s_LastSeenFileSystemTimeSampleFile).m_Timestamp;
    MutexUnlock(&s_LastSeenFileSystemTimeLock);
    return FileSystem::g_LastSeenFileSystemTime;
}
```
这一系列工作完成后，才会真正开始调用命令行执行当前节点Action中的具体操作。

### 临时文件目录

Tundra在构建过程中会生成一些临时文件，如Bee.dag_derived、tundra.scancache、tundra.digestcache等，都在路径<bee_dir>\artifacts下。

![image](IMG/temp_file.png)
