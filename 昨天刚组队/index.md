# 第二周：Unity构建基础 Bee 与 Tundra

Created: July 14, 2021 4:03 PM

## 知识点1 预备知识：引擎构建是怎么一回事？

材料：源代码，stevedore，编译器；

结果：引擎（.exe, .dll, etc.)

构建就是：从材料到结果的这个过程，如同流水线一样，关键是流水线的蓝图和装配机器：

装配机器：编译/下载/文件等指令

蓝图： 指令间的上下游依赖关系

一个简单的例子：

![images/1.png](images/1.png)

原材料-结果的逻辑关系

![images/2.png](images/2.png)

构建的工作：设计出对应的设备（指令）和流水线（依赖关系图），然后执行它

## Unity编译

### Unity编译相关工具

Perl：作为最外层的脚本语言，控制流程

Tundra2.exe：根据依赖关系执行构建

Bee可执行程序：生成依赖关系并调用Tundra2.exe构建

### 一个简单的流程

Perl触发构建：

Bee根据Perl参数生成 DAG图，描述构建流水线；

TUndra读入DAG图，执行指令完成构建；

DAG图：

```json
"Annotation": "Download and unpack artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/.StevedoreVersion (+1 other)",
"Action": "'External/MonoBleedingEdge/builds/monodistribution/bin/mono' --debug '/Users/xingweizhu/WorkSpace/Engine/unity/Tools/BeeBootstrap/bee_bootstrap.exe' steve internal-unpack public roslyn-csc-mac/8da8ba0cf075_43f799e85461479087dd2c2be93c600adeea8eae67e7570b1b5ef7b9952ab0ad.7z \"artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7\"",
"Inputs": [
       "artifacts/buildprogram/Stevedore/7za-mac-x64_1887/7za"
     ],
"Outputs": [
       "artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7/.StevedoreVersion",
       "artifacts/buildprogram/Stevedore/roslyn-csc-mac_43f7"
     ]
"Deps": [1, 2]
```

真实的构建流程:

![images/3.png](images/3.png)

全量Build的流程概况

### 一些概念

**构建图 Build graph**

构建图是构建系统需要做的工作的描述。图中的每个节点都是一个动作( Action )，它是构建系统可能需要执行的单个工作单元。图中的边是依赖关系，用于控制操作相对于彼此的执行顺序。

**前端 Frontend**

前端是构建系统中创建构建图的一部分。你写的所有 C# 构建代码都是前端的一部分。我们有时将该 C# 代码称为构建程序，它在运行时会输出一个包含构建图完整描述的 JSON 文件。

**后端 Backend**

后端也是构建系统的一部分，它接收构建图，检查磁盘上文件的状态，找出需要执行的操作，并实际执行这些操作的命令。它还负责决定何时需要运行前端。后端使用多线程和面向数据的编程技术，使构建图的评估速度极快。

**动作 Action**

每个动作至少具有以下内容：

- Action：一条需要运行的命令
- Output：一个或多个目标（也叫输出），这是该 Action 负责创建/更新的文件。运行该命令应该生成或更新所有列出的目标。每个目标必须是唯一的；两个 Action 不允许有相同的输出。
- Input：零个或多个输入。这些是决定 Action 是否需要重新运行的文件。如果 Action 的所有输出都存在，并且输入的时间戳（在某些情况下，比较SHA1哈希值）与 Action 最后一次运行时相同，那么它被认为是最新的；否则它将被重新运行。注意，在评估一个输出是否需要重建时，不考虑输出上的时间戳。

其他：

- 一个动作也可以指定对其他动作的依赖性。当动作 A 依赖于构建图中的动作 B 时，后端将拒绝处理动作 A，直到它完成对动作 B 的处理（无论是运行动作 B 的命令，还是确认输出已经是最新的）。例如，链接一个 C++ 程序的动作将依赖于将各个翻译单元编译为对象文件的动作。
- 稍微有点尴尬的是，即使依赖关系是在动作上，你在 API 中通过给出动作的输出来指定它们。然后，这些输出被用来查找动作，以建立它们之间的依赖关系。这意味着，如果你的动作有多个输出，而你想让其他动作依赖于该动作，你不需要在每个输出上设置依赖关系，只要其中一个就可以了。
- 请注意，输入和依赖不是一回事；如果动作 A 想要使用动作 B 生成的文件，那么它既要将该文件列为输入并声明对动作 B 的依赖。

### 分析 tundra_buildprogram.dag.json 里的所有指令和依赖关系

整个文件中的node根据先后顺序排序，每个node的结点参照DebugActionIndex，Index从0开始，累积直到最后一个为8。

- index为0的node，输入输出都为空，不进行讨论

    ```json
    "Annotation": "Download and unpack artifacts/buildprogram/Stevedore/7za-win-x64_a333/.StevedoreVersion (+1 other)",
          "Action": "\"D:\\source\\b\\unity\\Tools\\BeeBootstrap\\bee_bootstrap.exe\" steve internal-unpack public 7za-win-x64/38c5b39be2e8_a333cfccb708c88459b3812eb2597ca486ec9b416172543ca3ef8e5cd5f80984.zip \"artifacts/buildprogram/Stevedore/7za-win-x64_a333\"",
          "Inputs": [],
          "Outputs": [
            "artifacts/buildprogram/Stevedore/7za-win-x64_a333/.StevedoreVersion",
            "artifacts/buildprogram/Stevedore/7za-win-x64_a333"
          ],
          "TargetDirectories": [],
          "PassIndex": 0,
          "Deps": [],
          "OverwriteOutputs": true,
          "FrontendResponseFiles": [],
          "AllowUnexpectedOutput": true,
          "AllowUnwrittenOutputFiles": true,
          "AllowedOutputSubstrings": [],
          "Env": [
            {
              "Key": "BEE_INTERNAL_STEVEDORE_ARTIFACTVERSIONFILE",
              "Value": "artifacts\\buildprogram\\Stevedore\\7za-win-x64_a333\\.StevedoreVersion"
            }
          ],
          "DebugActionIndex": 1
    ```

    index为1

    "Annotation"中大致介绍了会进行什么动作，也提到了它的输出

    "Action": 拆包7za-win-x64/38c5b39be2e8_a333cfccb708c88459b3812eb2597ca486ec9b416172543ca3ef8e5cd5f80984.zip，生成7za-win-x64_a333及下属内容。

    "Inputs"是输入，在这里为空

    "Outputs"是输出，这里是7za-win-x64_a333/.StevedoreVersion及7za-win-x64_a333

    "Deps"是依赖，指明了Action之间的先后关系，这里为空，那么这个Action是第一个执行的，他不依赖任何输入就可以执行

  ```json
  "Annotation": "Download and unpack artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/.StevedoreVersion (+1 other)",
        "Action": "\"D:\\source\\b\\unity\\Tools\\BeeBootstrap\\bee_bootstrap.exe\" steve internal-unpack public referenceassemblies_v471/1_a0c33a9a16a4ad77c158aac7cd7e47daed3e68b180c65e14474f44c9501c4fae.7z \"artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3\"",
        "Inputs": [
          "artifacts/buildprogram/Stevedore/7za-win-x64_a333/7za.exe"
        ],
        "Outputs": [
          "artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3/.StevedoreVersion",
          "artifacts/buildprogram/Stevedore/referenceassemblies_v471_a0c3"
        ],
        "TargetDirectories": [],
        "PassIndex": 0,
        "Deps": [
          1
        ],
        "OverwriteOutputs": true,
        "FrontendResponseFiles": [],
        "AllowUnexpectedOutput": true,
        "AllowUnwrittenOutputFiles": true,
        "AllowedOutputSubstrings": [],
        "Env": [
          {
            "Key": "BEE_INTERNAL_STEVEDORE_7ZA",
            "Value": "D:\\source\\b\\unity\\artifacts\\buildprogram\\Stevedore\\7za-win-x64_a333\\7za.exe"
          },
          {
            "Key": "BEE_INTERNAL_STEVEDORE_ARTIFACTVERSIONFILE",
            "Value": "artifacts\\buildprogram\\Stevedore\\referenceassemblies_v471_a0c3\\.StevedoreVersion"
          }
        ],
        "DebugActionIndex": 2
  ```

  下载并拆包referenceassemblies_v471_a0c3/.StevedoreVersion

  这个过程拆包1_a0c33a9a16a4ad77c158aac7cd7e47daed3e68b180c65e14474f44c9501c4fae.7z，输入7za.exe，生成referenceassemblies_v471_a0c3/.StevedoreVersion，referenceassemblies_v471_a0c3等

  该动作依赖 DebugActionIndex 1，要在它之后执行，因为它的输入是上一步输出的内容


  ```json
  "Annotation": "Download and unpack artifacts/buildprogram/Stevedore/roslyn-csc-win64_b221/.StevedoreVersion (+1 other)",
        "Action": "\"D:\\source\\b\\unity\\Tools\\BeeBootstrap\\bee_bootstrap.exe\" steve internal-unpack public roslyn-csc-win64/8da8ba0cf075_b2212ef111874474f1da4a5359e3aa1fed09b11d5a0679f969b13a0f3d8c2709.7z \"artifacts/buildprogram/Stevedore/roslyn-csc-win64_b221\"",
        "Inputs": [
          "artifacts/buildprogram/Stevedore/7za-win-x64_a333/7za.exe"
        ],
        "Outputs": [
          "artifacts/buildprogram/Stevedore/roslyn-csc-win64_b221/.StevedoreVersion",
          "artifacts/buildprogram/Stevedore/roslyn-csc-win64_b221"
        ],
        "TargetDirectories": [],
        "PassIndex": 0,
        "Deps": [
          1
        ],
        "OverwriteOutputs": true,
        "FrontendResponseFiles": [],
        "AllowUnexpectedOutput": true,
        "AllowUnwrittenOutputFiles": true,
        "AllowedOutputSubstrings": [],
        "Env": [
          {
            "Key": "BEE_INTERNAL_STEVEDORE_7ZA",
            "Value": "D:\\source\\b\\unity\\artifacts\\buildprogram\\Stevedore\\7za-win-x64_a333\\7za.exe"
          },
          {
            "Key": "BEE_INTERNAL_STEVEDORE_ARTIFACTVERSIONFILE",
            "Value": "artifacts\\buildprogram\\Stevedore\\roslyn-csc-win64_b221\\.StevedoreVersion"
          }
        ],
        "DebugActionIndex": 3
  ```

  DebugActionIndex为3的node

  下载拆包roslyn-csc-win64_b221/.StevedoreVersion等

  这个过程拆包8da8ba0cf075_b2212ef111874474f1da4a5359e3aa1fed09b11d5a0679f969b13a0f3d8c2709.7z，输入7za.exe，生成referenceassemblies_v471_a0c3/.StevedoreVersion等

  依赖DebugActionIndex 1的输出

  ```json
  "Annotation": "WriteResponseFile artifacts/rsp/16678227308262555357.rsp",
        "WriteTextFilePayload": "命令",
        "Inputs": [],
        "Outputs": [
          "artifacts/rsp/16678227308262555357.rsp"
        ],
        "PassIndex": 0,
        "Deps": [],
        "OverwriteOutputs": true,
        "AllowUnwrittenOutputFiles": true,
        "DebugActionIndex": 4
  ```

  这是一种特殊的Action， 从名字可以看出来，WriteTextFilePayload，是将指令写入rsp文件。

  DebugActionIndex为4，输入空，输出16678227308262555357.rsp，无依赖

  ```json
  "Annotation": "Csc artifacts/buildprogram/buildprogram.exe (+2 others)",
        "Action": "\"artifacts\\buildprogram\\Stevedore\\roslyn-csc-win64_b221\\csc.exe\" /nostdlib /noconfig /shared @\"artifacts\\rsp\\16678227308262555357.rsp\"",
        "Inputs": [
          "*.cs",
          "*.exe",
          "*.dll",
          "artifacts/rsp/16678227308262555357.rsp"
        ],
        "Outputs": [
          "artifacts/buildprogram/buildprogram.exe",
          "artifacts/buildprogram/buildprogram.pdb",
          "artifacts/buildprogram/buildprogram.ref.exe"
        ],
        "TargetDirectories": [],
        "PassIndex": 0,
        "Deps": [
          2,
          3,
          4
        ],
        "OverwriteOutputs": true,
        "FrontendResponseFiles": [
          "artifacts/rsp/16678227308262555357.rsp"
        ],
        "AllowUnexpectedOutput": true,
        "AllowedOutputSubstrings": [],
        "SharedResources": [
          0
        ],
        "DebugActionIndex": 5
  ```

  编译生成buildprogram.exe等

  输入上一步生成的16678227308262555357.rsp以及referenceassemblies_v471_a0c3中的一些cs,exe,dll文件，输出buildprogram.exe，buildprogram.pdb，buildprogram.ref.exe

  依赖node 2，3，4

  ```json
  "Annotation": "CSProject Tools/Unity.BuildSystem/build.gen.csproj",
        "WriteTextFilePayload": "命令",
        "Inputs": [],
        "Outputs": [
          "Tools/Unity.BuildSystem/build.gen.csproj"
        ],
        "PassIndex": 0,
        "Deps": [],
        "OverwriteOutputs": true,
        "AllowUnwrittenOutputFiles": true,
        "DebugActionIndex": 6
  ```

  将指令写入build.gen.csproj，输入为空，输出build.gen.csproj，无依赖


  ```json
  "Annotation": "VisualStudioSolution Tools/Unity.BuildSystem/build.gen.sln",
        "WriteTextFilePayload": "命令",
        "Inputs": [],
        "Outputs": [
          "Tools/Unity.BuildSystem/build.gen.sln"
        ],
        "PassIndex": 0,
        "Deps": [
          6
        ],
        "OverwriteOutputs": true,
        "AllowUnwrittenOutputFiles": true,
        "DebugActionIndex": 7
  ```

  将指令写入build.gen.sln，无输入，输出build.gen.sln，依赖node 6

  ```json
  "Annotation": "build",
        "Action": "",
        "Inputs": [],
        "Outputs": [],
        "TargetDirectories": [],
        "PassIndex": 0,
        "Deps": [
          5,
          7
        ],
        "OverwriteOutputs": true,
        "FrontendResponseFiles": [],
        "AllowUnexpectedOutput": true,
        "AllowedOutputSubstrings": [],
        "DebugActionIndex": 8
  ```

  空Action，不讨论

以上结点的先后依赖顺序如下图所示：

![images/Untitled.png](images/Untitled.png)

# 知识点2 Perl流程控制

Tools/Build目录下的pl，pm文件。

入口是，jam.pl：

核心入口是：Tundra.pm里面的call_tundra

核心文件：Tundra.pm, frontend.pl, frontend.pm

## Perl构建

不带参数执行，build.pl 显示一个交互式菜单。 通过输入相应的数字然后按 Enter 来选择一个或多个要构建的目标。

![images/3.4.png](images/3.4.png)

使用以下命令格式传递参数（目标、选项）名称和值：

```json
perl build.pl --parameter=value [--parameter=value ...
```

使用其名称构建单个目标：

```json
perl build.pl --target=MacEditor
```

一次运行可以组合多个目标：

```json
perl build.pl --target=MacEditor,MacStandaloneSupport
```

要列出一些常见的构建选项，请执行 perl build.pl --help（检查脚本本身以获得完整的选项列表。）要查看选项的默认值，只需执行 build.pl 脚本。

```json
-codegen=release/debug
```

目标的发布或调试版本。主要启用编译器优化。还为特定于平台的行为设置 DEBUGMODE 和 UNITY_RELEASE 预处理器符号。默认设置为“debug”。构建机器通常将其设置为“release”。

```json
-developmentPlayer=0/1
```

对应于Editor中的“Development Build”复选框。生成更适合测试的二进制文件。此设置的真实和完整影响是高度特定于平台的。启用脚本调试、分析器、远程帧调试器、日志定义、可能减少优化，并可能在输出 Unity 应用程序中包含调试符号。注意：对编辑器构建没有影响；仅与玩家相关。

```json
-prepare / --scaffold
```

仅对所有目标执行必要文件的脚手架，因此目标在由 Xcode 或 Visual Studio 构建时将起作用。当准备命令提供 -target 列表时，只有那些目标会受到影响。

## Jam里面的脚本流程梳理及参数传递

![images/UnityBuildSystem.png](images/UnityBuildSystem.png)

1.配置 Stevedore manifest 文件供 bee_bootstrap 在编译主构建程序时使用。

2.  解析命令行参

3. Tundra DAG 文件名基于几乎所有传入的命令行参数，因为它们会影响 DAG 本身。

然而，只是将它们全部连接起来可能会导致文件名过长（尤其是在 Windows 上）。 因此，对常用参数进行一些常见的简短转换，并对其余参数进行散列：

- 构建目标名称：按原样通过

- CONFIG/PLATFORM/SCRIPTING_BACKEND/LUMP 参数：附加它们的值

- 其他所有内容：连接并附加所有 md5

例如：jam StandalonePlayer -sCONFIG=debug -sSCRIPTING_BACKEND=mono -sFOO=bar -sBAR=foo

最终使用的文件名为StandalonePlayerCfgdebugScriptmono-772f434d75.dag.json DAG

![images/5.1.png](images/5.1.png)

4. 处理处理非构建命令

- jam why 解释指定节点为何需要重新构建

- jam how 解释指定节点是如何构建的

- jam time-report 解释最近一次构建所需的时间

执行这些命令都要通过执行 Bee.StandaloneDriver.exe 得到结果

5. 安装构建过程依赖的工具

- PrepareMonoBleedingEdge  准备 mono 二进制文件

- PrepareExternalDependency("External/Unity.Cecil") External/Unity.Cecil对应包

- PrepareExternalDependency("External/il2cpp")External/il2cpp对应包

6. bee_bootstrap 用于在 Tundra 下调用自身时使用

7. 通常，我们生成 BuildSystemProjectFiles 作为准备构建程序的一部分 - 但如果构建程序现在没有编译，我们最终可能无法生成部分或全部项目文件，

因为一旦发现编译失败，Tundra 将停止构建更多节点。为了避免这种情况，我们明确地寻找构建BuildSystemProjectFiles 目标的请求，所以我们可以确保它被构建，即使构建代码本身不正确。

8. 设置clean的环境变量：

- Mac:配置SDKROOT环境变量
- Linux：设置LinuxSchroot
- Windows: 设置独立的环境变量

9. SetupLinuxSchroot：linux 构建过程中，它需要整个构建过程发生在“schroot”中，这是一个命令，可以将文件系统上的整个视图替换到不同的目录，并具有确切的编译器、链接器等。 这个函数描述了在任何 realcommand 之前放置什么前缀，以使其在这个 linux schroot 中发生。

10. 根据输入的命令行参数给cleanedIncomingArguments 变量赋值，根据cleanedIncomingArguments确定profile，dagfile等的文件名，然后根据前面获取到到变量结果调用call_tundra

```json
call_tundra($schrootprefix, $dagfile, "perl Tools/Unity.BuildSystem/frontend.pl $arg_string", join(" ", @tundraargs));
```

### 知识点3：Tundra

以下研究基于2020.3/staging上源码版本对应的Tundra展开：

- tundra2.exe寻源之路：
    - 在Unity源码执行prepare之后，可从`artifacts\tundra-via-stevedore-in-perl\`中找到`tundra2.exe`并通过执行以下命令得到tundra2.exe对应的源码所在branch和commit。

        ```bash
        $ ./artifacts/tundra-via-stevedore-in-perl/tundra2.exe --version

          Tundra Build Processor 2.0
          Copyright (C) 2010-2018 Andreas Fredriksson
          
          Git branch: $3102ff8c2c757069d56dccace551f48020864ddc
          Git commit: $3102ff8c2c757069d56dccace551f48020864ddc:origin/DependenciesConsumedDuringUsageOnly
        ```

    - 然后，我们可以发现，[新repo]([https://github.cds.internal.unity3d.com/unity/bee/](https://github.cds.internal.unity3d.com/unity/bee/)) 中并无此提交，而[旧Repo]([https://github.com/Unity-Technologies/tundra](https://github.com/Unity-Technologies/tundra))中则包含此提交（尽管此提交并不在DependenciesConsumedDuringUsageOnly分支上，而是在various_unity分支上）
    - 克隆工程，并切换到 commit 3102ff8c2c757069d56dccace551f48020864ddc

        ```bash
        $ git clone git@github.com:Unity-Technologies/tundra.git
        $ git checkout 3102ff8c2c757069d56dccace551f48020864ddc
        ```

    - 初始化googletest子模块

        ```bash
        $ git submodule update --init --recursive
        ```

    - tundra源码结构

        ![images/Untitled%201.png](images/Untitled%201.png)

    - 修复代码错误

        ![images/Untitled%202.png](images/Untitled%202.png)

    - 调用bee进行构建

        ```bash
        $ ./bee.exe
          [             ] Require frontend run.  tundra.dag no longer valid. directory contents changed: unittest
          [             ] Require frontend run.  tundra_buildprogram.dag no longer valid. directory contents changed: .
          [           0s] Freezing tundra_buildprogram.dag.json into .dag (.dag file didn't exist)
          *** buildprogram build success (0.03 seconds), 0 items updated, 9 evaluated
          [        0s] Executed build program. Created build graph with 199 nodes.
          [           0s] Freezing tundra.dag.json into .dag (.dag file didn't exist)
          [  5/199    0s] WriteText visualstudio/tundra2-unittest.gen.vcxproj.filters
          [ 12/199    0s] WriteText visualstudio/tundra2-unittest.gen.vcxproj
          [ 61/199    1s] C_Win64_VS2019 artifacts/tundra2-unittest/master_Win64_VS2019_nonlump/g6ki/Test_IncludeScanner.obj
          ....
          [141/199    0s] C_Win64_VS2019 artifacts/tundra2-unittest/debug_Win64_VS2019_nonlump/g6ki/Test_StripAnsiColors.obj
          [142/199    0s] Link_Win64_VS2019 artifacts/tundra2-inspect/debug_Win64_VS2019_nonlump/tundra2-inspect.exe (+pdb)
          [143/199    0s] Link_Win64_VS2019 artifacts/t2/tundra2/debug_Win64_VS2019_nonlump/tundra2.exe (+pdb)
          ...
          [153/199    0s] CopyTool build/windows-x86_64/debug/tundra2.exe
          ...
          [pram] Runner environments for the given identifier:
          [pram] - "Windows": fully available, priority 1
          [pram] Launching application "D:/github/UnityTechnologies/tundra/build/windows-x86_64/debug/tundra2-unittest.exe" on runner environment "Windows"...
          [==========] Running 47 tests from 13 test cases.
          [----------] Global test environment set-up.
          [----------] 1 test from BitFuncs
          [ RUN      ] BitFuncs.PopLsb
          [       OK ] BitFuncs.PopLsb (0 ms)
          [----------] 1 test from BitFuncs (0 ms total)
          ...
          [  PASSED  ] 47 tests.
          [pram] Done launching and reading results, total time: 00:00:00.0349716
          [pram] Application quit with exit code 0
          [           0s] Destroying MsPDBSrv (16.4)
          [           0s] Destroying Visual Studio Telemetry (VCTIP.EXE)
          *** Bee build success (8.84 seconds), 51 items updated, 199 evaluated
        ```

        构建完成即可在`build\windows-x86_64\master` 和 `build\windows-x86_64\debug`下生成`tundra2.exe`

    - 测试

        将上述步骤生成的`trundra2.exe`替换到Unity源码**artifacts\tundra-via-stevedore-in-perl**和**artifacts\tundra-via-stevedore**中，可以正常构建出Editor和Player

    - 进一步探究
        - 此处的bee.exe从何而来？

            ![images/Untitled%203.png](images/Untitled%203.png)

            从IL Code的结构中可以看出，用于构建tundra2的工具 bee.exe实际上就是完整的bee, 和Tools\BeeBootstrap\bee_bootstrap.exe完全是相同的东西。

            - 佐证

                ```bash
                $ Tools/BeeBootstrap/bee_bootstrap.exe --version
                > Bee buildsystem. Powered by Tundra by Andreas Fredriksson.  
                > Built from: 2b54f3e9094b (2020.2/platform/mac/macsdkprovider-sync)

                $ github/UnityTechnologies/tundra/bee.exe --version 
                > Bee buildsystem. Powered by Tundra by Andreas Fredriksson.  
                > Built from: ef716c6ae229 (trunk)

                ```

                抛开版本问题，两者都是从Unity源码中的 Tools/Bee 构建而来。

### 问题回答

- **如何实现按照依赖关系顺序Build 以及 如何实现增量构建**
    - Dag任务节点状态机

        ![images/Untitled%204.png](images/Untitled%204.png)

        参考：[https://github.com/Unity-Technologies/tundra/tree/3102ff8c2c757069d56dccace551f48020864ddc/src](https://github.com/Unity-Technologies/tundra/tree/3102ff8c2c757069d56dccace551f48020864ddc/src)  之 BuildLoop.cpp 和 RunAction.cpp 

    - **如何按照依赖关系顺序构建**

        从tundra/src/BuildQueue.cpp可知：

        （1）tundra使用一个数组存储任务节点队列（BuildQueue.hpp ->BuildQueue::m_Queue）

        （2）创建多个线程用于执行任务(BuildQueue::BuildQueueInit)

        （3）每个线程不断查找下一个未执行的任务节点，并执行它，直至没有任务需要执行 (BuildLoop::BuildLoop)

        （4）在执行每个任务节点时，如果其依赖的任务节点未完成，则将其依赖任务加入任务队列，并将当前任务加锁 (BuildLoop::ProcessNode)

        （5）当执行完每个任务节点时，会检查依赖该节点的任务现在是否可以解锁 (BuildLoop::FinishNode)

    - **如何实现增量构建**
        - 概述：

            对于每个节点，会拿现有的**签名信息** 和上一次构建时保存的签名信息进行对比，如果满足

            （1） 签名一致

            （2）上一次构建的结果还在

            则跳过执行此任务，否则会重新执行该任务。

        - 签名信息定义及作用

            **签名信息**是一种用于检查工程文件过期情况的模型，是一种比较通用、稳健的模型。参考

            代码**HashDigest.cpp**、LeafInputSignature.cpp

            tundra中的签名信息主要包括：

            (1) 文件时间戳

            (2) 文件内容hash

            (3) 输入文件或头文件等依赖文件的签名信息。

            ![images/Untitled%205.png](images/Untitled%205.png)

        - 如何找到上一次构建的签名信息

            tundra会使用GUID（Globally Unique Identifier，全局唯一标识符）记录任务节点对应的签名信息。

            每次构建完成后，tundra都会将所有任务节点的签名保存至**artifacts/TundraBuildState.state** 文件，下次构建时，将根据节点GUID加载 该任务节点的“先人签名” 用以比较签名。

### 知识点3：Bee

### Bee 的简介

Bee 是一个构建系统并被多个 Unity 的项目采用，Unity Editor 只是其中一个项目，更多项目可以在 [https://confluence.unity3d.com/display/BST/Bee+user+base](https://confluence.unity3d.com/display/BST/Bee+user+base) 查看，Bee 的一些主要特点有：

- 构建逻辑使用 **C#** 编写：
    - Bee 提供了用于描述如何构建 C++ 和 C# 程序的 C# API，并支持 Unity 使用的**许多平台和编译器工具链**。
    - 需要注意的是，C# 构建代码最终只会创建构建图；但实际上并没有执行它。
- 在仅修改源代码文件的情况下，再次迭代将会相当快速
    - 大多数情况下不执行 C# 逻辑
    - 底层的低级构建后端是基于 Tundra 构建系统（Unity fork）
- 会生成常见 IDE （如 Visual Studio 和 Xcode）的项目文件
- Bee 以 bee.zip 或 bee-NDA.zip 的形式发布时，包括对 NDA 平台的支持
    - 唯一需要的依赖是一个 .NET runtime （Windows 平台是 .NET 4.6，其他平台则是 Mono 5.x）
    - 各种辅助工具（如 Bee backend）会打包在 bee.zip 里面

> 关于隐私
Unity 在 Bee 构建期间收集技术信息和遥测数据， 可以运行 `./bee privacy` 以获取更多信息。

 StandaloneBeeDriver.cs 源码

- 首先是 Main 函数，会去调用 RealMain 函数。
- RealMain 主要是处理 TopLevelBeeCommand 指令，如 `--help` 和 `--version` 以及 `how` 这种最顶级的指令，如果没有 TopLevelBeeCommand 再执行真正的 build 操作。

    > RealMain 函数中会先用 GlobalOptions 去解析运行的参数，当检查到第一个参数为--help 或 -h 时会调用 ShowBeeHelp。把后续的参数传入，先调用 ShowBeeVersion 打印版本信息，再通过遍历后续参数后打印相关的描述和提示信息。如果第一个参数为 --version 或 -v 则直接调用 ShowBeeVersion 打印版本信息。如果是其他情况的顶级的参数则执行对应的调用（例如 how），如果没有其他的顶级参数，则执行默认的 build，可以查看到 build 对应的文件为同目录下的 BuildTopLevelBeeCommand.cs ，执行的操作为调用 BuildMain 操作。

- 至此我们找到了执行 build 的入口函数 BuildMain 。函数的开头就注释了一段文字描述 bee 和 tundra 如何协同工作的。简单来说就是用户去调用 bee ，然后 bee 调用 tundra ，tundra 负责根据最新的数据，立即执行构建。

接下来是一段结合代码的注释：

```csharp
//该函数是 StandaloneBeeDriver 执行 build 的入口函数
public static void BuildMain(string[] args)
{
    //生成一个 session ID ，确保同一次构建中产生的子进程也继承相同的 session ID
    TelemetrySupport.BeginTopLevelSession();
    //添加指令到 BeeOptions.extra 中
    BeeOptions.ParseCommandLine(args);
    //在当前路径中读取 bee.config 文件
    Config = LoadConfiguration(BeeOptions.BuildProgramRoot);
    //设置当前目录路径
    Directory.SetCurrentDirectory(BeeOptions.BuildProgramRoot.MakeAbsolute().ToString(SlashMode.Native));
    //打印彩色化相关设置
    if (ConsoleColorUtility.ShouldWeOutputColors && !BeeOptions.disableColors)
        Environment.SetEnvironmentVariable(BeeEnvVar.CanHandleColors, "1");
    if (BeeOptions.disableColors)
        Environment.SetEnvironmentVariable(BeeEnvVar.CanHandleColors, "0");
    //检测 Tundra 是否在 artifacts/tundra-via-stevedore 目录下，如果没有的，下载 Tundra
    TundraInvoker.EnsureTundraConfiguredInEnvironment(Configuration.AbsoluteRootArtifactsPath.Combine("tundra-via-stevedore"));
    //添加 KnuthHash 到 BeeOptions.extra 中
    var hash = new KnuthHash();
    hash.Add(BeeOptions.extra);
    //根据 Config 判断是否有多个 dag 文件
    var multiDag = Config.TryGetValue("MultiDag", out var token) && token.Value<bool>();
    //如果有多个 dag 文件，按要求生成文件路径，如果没有则路径为 artifacts/tundra.dag
    NPath buildGraphDagFile = multiDag ? Configuration.RootArtifactsPath.Combine($"tundra_{BeeOptions.extra.FirstOrDefault() ?? string.Empty}_{hash.GetStringValue(3)}.dag") : BuildGraphDagFile;

    var targetsToRequest = BeeOptions.onlyTarget == null
        ? BeeOptions.extra
        : new List<string>() {BeeOptions.onlyTarget};
    //调用 Tundra，其中第一个参数 buildGraphDagFile 为上面生成的 dag 文件路径, 第三个参数用来通过 BeeOptions 中的设置生成 Tundra 的对应参数 第二个参数是用来生成 Json 的 Action，在下面分析
    InvokeTundra(buildGraphDagFile, CreateBuildGraph, BeeOptionsTranslatedToTundraBehaviourFlags.Concat(targetsToRequest).ToArray());
    //验证是否为最新的构建
    if (BeeOptions.verifyUpToDate)
    {
        //通过读 artifacts/tundra.log.json 进行重建
        var explainer = new Why.TundraExplainer(Configuration.RootArtifactsPath);
        if (explainer.AllBuiltNodes.Any())
        {
            foreach (var n in explainer.AllBuiltNodes)
            {
                var explanation = explainer.Explain(n);
                explanation.FormatToConsole();
            }

            Environment.Exit(1);
        }
    }
}

//调用 TundraInvoker 的一个封装函数，通过 produceJson 这个 Action 来写 *.dag.json 文件
private static void InvokeTundra(NPath tundraDag, Action<NPath> produceJson, IEnumerable<string> tundraArgs)
{
    NPath jsonFile = tundraDag + ".json";
    //是否需要重新生成图
    if (BeeOptions.forceRecreateDepgraph)
    {
        tundraDag.MakeAbsolute().DeleteIfExists();
        jsonFile.MakeAbsolute().DeleteIfExists();
        produceJson(jsonFile);
    }

    while (true)
    {
        //通过 TundraInvoker 调用 Tundra
        var result = TundraInvoker.Invoke(TundraInvoker.TundraBinaryPath, tundraDag, NPath.CurrentDirectory,
            Shell.StdMode.Stream, false, tundraArgs);
        //判断当前流程状态 ExitCode_RerunFrontend 代表回到前端构建
        if (result.ExitCode == TundraInvoker.ExitCode_RerunFrontend)
        {
            jsonFile.MakeAbsolute().DeleteIfExists();
            produceJson(jsonFile);
            continue;
        }
        //result.ExitCode == 0 代表成功，其他 ExitCode 通过 Environment.Exit 返回
        if (result.ExitCode != 0)
            Environment.Exit(result.ExitCode);

        return;
    }
}

//根据程序所有的 AllNodes 节点结构写入到 json 文件中
private static void CreateBuildGraph(NPath targetJson)
{
    // 第一个参数为 artifacts/buildprogram/tundra_buildprogram.dag 这个步骤就是用来生成 buildprogram 所需要的 .dag.json
    InvokeTundra(BuildProgramDagFile, CreateGraphForBuildProgram, BeeOptionsTranslatedToTundraBehaviourFlags);

    var stopWatch = new Stopwatch();
    stopWatch.Start();

    //转换 bee 构建参数为 shell 参数
    BeeBuildCommand.ExecuteToShellArgsConverter = beeBuildCommand =>
    {
        var flags = new StringBuilder();
        if (beeBuildCommand.Rebuild)
            flags.Append("-l");
        if (!beeBuildCommand.Colors)
            flags.Append("--no-colors");

        return new Shell.ExecuteArgs
        {
            Executable = BeeInvocationString,
            Arguments = $"{flags} {beeBuildCommand.Targets.SeparateWithSpace()}",
            WorkingDirectory = Paths.ProjectRoot.ToString()
        };
    };
    // 加载 buildprogram 执行程序
    var assembly = Assembly.LoadFrom(BuildProgramPath.ToString(SlashMode.Native));
    if (assembly.EntryPoint == null)
        throw new InvalidOperationException("BuildProgram has no entry point.");

    var tundraBackend = new TundraBackend.TundraBackend(Configuration.RootArtifactsPath, "Bee");

    using var _ = tundraBackend.InstallRequirementsForRunningBuildCode();

    assembly.EntryPoint.Invoke(null, Array.Empty<object>());

    int seconds = (int)stopWatch.Elapsed.TotalSeconds;
    var whitespace = new string(' ', 8 - (int)Math.Ceiling(Math.Log10(seconds + 1)));
    Console.WriteLine($"[{whitespace}{seconds}s] Executed build program. Created build graph with {tundraBackend.ActionCount} nodes.");

    tundraBackend.AddInputsFromOtherTundraFileAsFileSignatures(BuildProgramDagFile.ChangeExtension(".dag.json"));
    tundraBackend.AddFileAndGlobSignaturesFromOtherTundraFile(BuildProgramDagFile.ChangeExtension(".dag.json"));

    tundraBackend.AddExtensionToBeScannedByHashInsteadOfTimeStamp("c", "cpp", "h", "cs", "hpp");
    // 根据 AllNodes 写入结构到 json 文件中
    tundraBackend.Write(targetJson);
}

//根据 自定义构建文件 或者 预设构建文件 生成对应的 buildprogram 的 .dag.json 文件
private static void CreateGraphForBuildProgram(NPath targetJson)
{
    //是否使用自定义的构建文件
    var usingCustomBuildProgram = Config.TryGetValue("BuildProgramBuildProgramFiles", out var _);

    if (usingCustomBuildProgram)
    // The buildprogram buildprogram should be created by compiling and running the files given in the config
        //如果使用自定义的构建文件 artifacts/buildprogram/buildprogram_buildprogram.dag，则需要构建文件中定义的文件 CreateGraphForBuildProgramBuildProgram 配置 tundraBackend 构建，写 *.dag.json 文件
        InvokeTundra(BuildProgramBuildProgramDagFile, CreateGraphForBuildProgramBuildProgram, BeeOptionsTranslatedToTundraBehaviourFlags);
    //设置构建设置/初始化代码
    MetaBuildSettings.BeginMetaBuildSetup(out var tundraBackend, usingCustomBuildProgram);
    using var _ = tundraBackend.InstallRequirementsForRunningBuildCode();
    if (usingCustomBuildProgram)
    {
        tundraBackend.AddInputsFromOtherTundraFileAsFileSignatures(BuildProgramBuildProgramDagFile.ChangeExtension(".dag.json"));
        tundraBackend.AddFileAndGlobSignaturesFromOtherTundraFile(BuildProgramBuildProgramDagFile.ChangeExtension(".dag.json"));

        BeeBuildCommand.ExecuteToShellArgsConverter = beeBuildCommand =>
        {
            var flags = new StringBuilder();
            if (beeBuildCommand.Rebuild)
                flags.Append("-l");

            return new Shell.ExecuteArgs
            {
                Executable = BeeInvocationString,
                Arguments = $"{flags} {beeBuildCommand.Targets.SeparateWithSpace()}",
                WorkingDirectory = Paths.ProjectRoot.ToString()
            };
        };

        var assembly = Assembly.LoadFrom(BuildProgramBuildProgramPath.ToString(SlashMode.Native));
        if (assembly.EntryPoint == null)
            throw new InvalidOperationException("BuildProgram BuildProgram has no entry point function.");
        assembly.EntryPoint.Invoke(null, new object[] {new[] {BuildProgramPath.ToString()}});
    }
    else
    {
        //如果没有自定义的 build 文件，则根据 Bee.Examples 生成相关文件
        SetupDefaultBuildProgramBuildProgram();
        RegisterFrontendInfluencingFilesWithTundraBackend(tundraBackend);
    }
    //根据 tundraBackend 的构建关系，写 *.dag.json 文件
    tundraBackend.Write(targetJson);
}
```

根据上面的分析我们可以回答下列问题：

- buildprogram 是如何生成的？
buildprogram 是通过 CreateGraphForBuildProgram 生成 tundra_buildprogram.dag.json ，然后通过这个 json 文件配合 tundra 生成 buildprogram。
- Bee 调用 buildprogram 的 Main 函数的细节？
这个 Main 函数具体实现代码在 `unity/Tools/Bee/Bee.BuildProgram/BuildProgramForBee.bee.cs` 文件中的 `SetupBuildProgramBuildProgram` 函数内。
- Bee 是如何用参数指定构建文件`*.bee.cs`的路径？
    - 在 StandaloneBeeDriver 的加载 bee 的配置文件 LoadConfiguration 中，发现会去找一个 `bee.config`  的文件，  在目录`unity/Tools/Bee/`下找到了这个 `bee.config` 文件。

    ```csharp
    {
      "BeeFiles": ["Bee.BuildProgram"]
    }
    ```

    - 其中`BeeFiles` 的参数是路径 `Bee.BuildProgram`，通过这个参数，找到的 Bee.BuildProgram/Build.bee.cs. 而这也是最初的 buildprogram 的执行文件。
    - 所以可以得出 Bee 会通过 `bee.config` 文件中的 `BeeFiles` 的参数，来确定文件路径 或者 文件所在路径，进行添加和构建。
- bee_bootstrap.exe与Bee.StandaloneDriver.exe 之间的区别
    - bee_bootstrap.exe的源文件和依赖
        - 在artifacts/tundra.dag.json中可以找到构建bee_bootstrap.exe所需要的原料
            - 原料

                Bee.Tools.dll

                Bee.Stevedore.Program.exe

                Bee.Core.dll

                Bee.DotNet.dll

                Bee.VisualStudioSolution.dll

                Bee.CSharpSupport.dll

                Bee.CSharpSupport.Unity.dll

                Bee.TundraBackend.dll

                Bee.Why.dll

                Bee.StandaloneDriver.exe

                Bee.BuildTools.dll

                Bee.UnityTools.dll

                NiceIO.dll

            - 外部库

                Newtonsoft.Json.dll

            - 输出

                artifacts/Bee/Distributions/757756874/bee_bootstrap.exe

            - 工具

                artifacts/Stevedore/ilrepack_9a20/ILRepack.exe

    - Bee.StandaloneDriver.exe的源文件和依赖库

        参考文件：artifacts\tundra.dag.json

        - 源文件

            Tools/Bee/Bee.StandaloneDriver/***.cs

        - 外部依赖库

            .Net_v471

            Newtonsoft.Json.dll

            Bee.CSharpSupport.dll

            Bee.DotNet.dll

            Bee.Core.dll

            Bee.TundraBackend.dll

            NiceIO.dll

            Bee.Tools.dll

            Bee.VisualStudioSolution.dll

            Bee.Why.dl

            Bee.Stevedore.Program.exe

        - 工具

            csc.exe

    - bee_bootstrap.exe与Bee.StandaloneDriver.exe的组成对比

        虽然bee_bootstrap.exe由一堆dll和Bee.StandaloneDriver.exe共同构建而成，但是bee_bootstrap.exe的原料dll大多已经被Bee.StandaloneDriver.exe包含。

        ![images/Untitled%206.png](images/Untitled%206.png)

        - 分析

            bee_bootstrap.exe 只比 Bee.StandaloneDriver.exe 多了Bee.CSharpSupport.Unity.dll、Bee.BuildTools.dll和Bee.UnityTools.dll。

            其中， Bee.CSharpSupport.Unity.dll和Bee.UnityTools.dll与Unity相关，而Bee.BuildTools.dll仅仅包含源码所在git/hg版本记录工具和Zip压缩工具。

### **知识点5:** Unity的Build简单流程梳理

![images/Untitled%207.png](images/Untitled%207.png)

Unity构建简单流程如图所示，主要分为以下步骤：

- 用bee和bee.cs生成buildprogram.exe
- 用buildprogram.exe和jam.cs等文件，生成一个包含源码结构信息的Unity.BuildSystem.exe
- 最后用Unity.BuildSystem.exe结合具体的源码进行最终的构建，生成Editor或者Player

### 问题回答：

- **参考dag文件，整理buildprogram.exe的源文件和外部库**

    参考：artifacts\buildprogram\tundra_buildprogram.dag.json

    - 源文件
        - Tools/Unity.BuildSystem/Unity.BuildSystem.BuildProgramForBeeUnity/BuildProgramForBeeUnity.cs
        - Tools/Bee/Bee.BuildProgram/BuildProgramForBee.bee.cs
    - 外部库
        - Tools/BeeBootstrap/bee_bootstrap.exe
        - .Net-v471
    - 工具
        - roslyn-csc
- **参考dag文件，整理Unity.BuildSystem.exe的源文件和外部库**

    参考：artifacts\tundra.dag.json

    - 源文件
        - Tools/Unity.BuildSystem/Unity.BuildSystem/*.cs
        - Platforms/*****/Unity.BuildSystem
            - Platforms/GameCore/Unity.BuildSystem
            - Platforms/（PS4/PS5/XBOX....）/Unity.BuildSystem
        - *.jam.cs
        - PlatformDependent/**PLATFORM**/Jam/*.cs
    - 内部依赖库
        - Bee.UnityTools.dll
        - NiceIO.dll
        - Bee.****.exe/dll
    - 外部库
        - .Net-v471
        - Newtonsoft.Json.dll
    - 工具
        - roslyn-csc

### 知识点6 套娃之王？

- tundra的依赖关系

    ![images/Untitled%208.png](images/Untitled%208.png)

- 到底有没有套娃？
    - 观点：没有套娃
    - 依据：
        - tundra2作为一个独立的Native程序，结构非常简单。尽管unity的tundra2使用bee构建，但是tundra2本身并不包含bee。且即便不使用bee，使用CMake等构建工具或者使用脚本，也能构建出tundra2。
        - 从外观上看，bee中包含了tundra, 但是bee不必包含tundra。
            - 从bee项目源码结构分析

                ![images/Untitled%209.png](images/Untitled%209.png)

                bee的项目中包含了tundra的源码，并且Build.bee.cs中添加了tundra的NativeProgram构建项，但是只是顺手而为。bee的源码和tundra的源码各自会构建出不同的可执行程序。

            - 从使用bee过程分析

                ![images/Untitled%2010.png](images/Untitled%2010.png)

                在使用了bee的第三方项目中，工程只包含 工程本身源码和bee两样，tundra并不包含在项目中。在使用bee构建时，会从stevedore上下载tundra2。

## 附录

### 参考资料

- About Stevedore

    [Stevedore - Package Management System](https://stevedore.unity3d.com/)

    [Stevedore - Package Delivery Service](https://confluence.unity3d.com/display/BST/Stevedore+-+Package+Delivery+Service)

    [Development Guide (Stevedore - Package Manager)](https://confluence.unity3d.com/pages/viewpage.action?pageId=102667591)

    [Project Guide (Stevedore - Package Manager)](https://confluence.unity3d.com/pages/viewpage.action?pageId=105762428)

- About Jam

    [Jam/Build - Build System](https://internaldocs.hq.unity3d.com/build_system/)

- About Bee

    [Sign in to Unity](https://unity.enterprise.slack.com/files/W018X197MU3/FRSV0U7B9/bee_introductionvideo.mp4)

    [Bee - Build System](https://internaldocs.hq.unity3d.com/bee_build_system/)

    [Bee - Build System Tool](https://confluence.unity3d.com/display/BST/Bee+-+Build+System+Tool)

    [Bee - user base](https://confluence.unity3d.com/display/BST/Bee+user+base) 

    [Bee - GitHub](https://github.cds.internal.unity3d.com/unity/bee)

    [Debugging Bee](https://confluence.unity3d.com/display/BST/Debugging+Bee)

- About Tundra

    [Tundra - Build Tool](https://confluence.unity3d.com/display/BST/Tundra+-+Build+Tool)
    [Debugging Tundra](https://confluence.unity3d.com/display/BST/Debugging+Tundra)

### perl脚本单步调试

- 安装IDEA
- 安装Perl插件
    - File → Settings → Plugins

    ![images/Untitled%2011.png](images/Untitled%2011.png)

- 使用Idea打开Unity源码 (根目录)
- 添加Perl解释器： File→Settings→Languages&FrameWorks→Perl5

    ![images/Untitled%2012.png](images/Untitled%2012.png)

- 添加调试项
    - 打开 "Edit Configurations" 页面

        ![images/Untitled%2013.png](images/Untitled%2013.png)

    - 添加perl命令和参数

        ![images/Untitled%2014.png](images/Untitled%2014.png)

- 添加断点，并开启调试

    ![images/Untitled%2015.png](images/Untitled%2015.png)

- 跟踪变量值

    ![images/Untitled%2016.png](images/Untitled%2016.png)

- 单步调试

    ![images/Untitled%2017.png](images/Untitled%2017.png)